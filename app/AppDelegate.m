//
// Copyright (c) 2016 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "AppDelegate.h"
#import "ChatsView.h"
#import "PeopleView.h"
#import "GroupsView.h"
#import "SettingsView.h"

#import "NavigationController.h"
#import <VENTouchLock/VENTouchLock.h>
#import "PasscodeViewController.h"

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate>
@end
#endif


@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";


//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	//---------------------------------------------------------------------------------------------------------------------------------------------
	// Crashlytics initialization
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[Fabric with:@[[Crashlytics class]]];
	//---------------------------------------------------------------------------------------------------------------------------------------------

	//---------------------------------------------------------------------------------------------------------------------------------------------
	// Firebase initialization
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[FIRApp configure];
	[FIRDatabase database].persistenceEnabled = NO;
	//---------------------------------------------------------------------------------------------------------------------------------------------

	//---------------------------------------------------------------------------------------------------------------------------------------------
	// Facebook login initialization
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
	//---------------------------------------------------------------------------------------------------------------------------------------------

	//---------------------------------------------------------------------------------------------------------------------------------------------
	// Push notification initialization
	//---------------------------------------------------------------------------------------------------------------------------------------------
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [application registerForRemoteNotifications];
    
    [FIRMessaging messaging].delegate = self;

	//---------------------------------------------------------------------------------------------------------------------------------------------
	// OneSignal initialization
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[OneSignal initWithLaunchOptions:launchOptions appId:ONESIGNAL_APPID handleNotificationReceived:nil handleNotificationAction:nil
							settings:@{kOSSettingsKeyInAppAlerts:@NO}];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[OneSignal setLogLevel:ONE_S_LL_NONE visualLevel:ONE_S_LL_NONE];
	//---------------------------------------------------------------------------------------------------------------------------------------------


	if ([UserDefaults boolForKey:@"Initialized"] == NO)
	{
		[UserDefaults setObject:@YES forKey:@"Initialized"];
		[FUser logOut];
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------

	//---------------------------------------------------------------------------------------------------------------------------------------------
	// Reachability initialization
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[Connection shared];
	//---------------------------------------------------------------------------------------------------------------------------------------------

	//---------------------------------------------------------------------------------------------------------------------------------------------
	// Realm initialization
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[Groups shared];
	[Recents shared];
	[Users shared];
	[UserStatuses shared];
	//---------------------------------------------------------------------------------------------------------------------------------------------

	//---------------------------------------------------------------------------------------------------------------------------------------------
	// UI initialization
	//---------------------------------------------------------------------------------------------------------------------------------------------
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

	self.chatsView = [[ChatsView alloc] initWithNibName:@"ChatsView" bundle:nil];
	//self.callsView = [[CallsView alloc] initWithNibName:@"CallsView" bundle:nil];
	self.peopleView = [[PeopleView alloc] initWithNibName:@"PeopleView" bundle:nil];
	self.friendRequestsView = [[FriendRequests alloc] initWithNibName:@"FriendRequests" bundle:nil];
	
    self.settingsView = [[SettingsView alloc] initWithNibName:@"SettingsView" bundle:nil];
    self.settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    
    
    self.friendRequestsListView = [[FriendRequesListView alloc] initWithNibName:@"FriendRequesListView" bundle:nil];
    
    
    self.groupsView = [[GroupsView alloc] initWithNibName:@"GroupsView" bundle:nil];
    //NavigationController *navGroup = [[NavigationController alloc] initWithRootViewController:self.groupsView];

	NavigationController *navController1 = [[NavigationController alloc] initWithRootViewController:self.chatsView];
	
    //NavigationController *navController2 = [[NavigationController alloc] initWithRootViewController:self.callsView];
    NavigationController *navController2 = [[NavigationController alloc] initWithRootViewController:self.friendRequestsListView];
	
    NavigationController *navController3 = [[NavigationController alloc] initWithRootViewController:self.peopleView];
	NavigationController *navController4 = [[NavigationController alloc] initWithRootViewController:self.friendRequestsView];
	
    //NavigationController *navController5 = [[NavigationController alloc] initWithRootViewController:self.settingsView];
    NavigationController *navController5 = [[NavigationController alloc] initWithRootViewController:self.settingsViewController];

	self.tabBarController = [[UITabBarController alloc] init];
	self.tabBarController.viewControllers = @[navController2, navController4, navController3, navController1, navController5];
	self.tabBarController.tabBar.translucent = NO;
	self.tabBarController.selectedIndex = DEFAULT_TAB;

	self.window.rootViewController = self.tabBarController;
	[self.window makeKeyAndVisible];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[self.chatsView view];
    [self.friendRequestsView view]; //[self.callsView view];
	[self.peopleView view];
	[self.friendRequestsListView view];
	[self.settingsViewController view];
    
    //[self.groupsView view];
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
	//Check if the time_session has been set.
    if(![UserDefaults integerForKey:TIME_SESSION_KEY]){
        [UserDefaults setObject:@(DEFAULT_TIME_SESSION) forKey:TIME_SESSION_KEY];
    }
    
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    NSLog(@"%@",[config.fileURL absoluteString]);
    
    
    NSInteger num_intentos = 1000;
    NSNumber *intentos = [[NSUserDefaults standardUserDefaults] objectForKey:@"KEYCHAIN_NUMERO_DE_INTENTOS"];
    if(intentos){
        num_intentos = intentos.integerValue;
    }
    
    [[VENTouchLock sharedInstance] setKeychainService:@"KEYCHAIN_SERVICE_NAME"
                                      keychainAccount:@"KEYCHAIN_ACCOUNT_NAME"
                                        touchIDReason:@"Escanea tu huella para usar la App"
                                 passcodeAttemptLimit:num_intentos
                            splashViewControllerClass:[PasscodeViewController class]];
    
    
	return YES;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)applicationWillResignActive:(UIApplication *)application
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)applicationDidEnterBackground:(UIApplication *)application
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[self locationManagerStop];
    

    
    if([FUser currentId]){
        FIRDatabaseReference * firebase = [[FIRDatabase database] referenceWithPath:FRECENT_PATH];
        //FIRDatabaseQuery *query = [[firebase queryOrderedByChild:FRECENT_USERID] queryEqualToValue:[FUser currentId]];


        [firebase observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot *snapshot)
         {
             NSLog(@"BBBB");
             
         }];

        [firebase observeEventType:FIRDataEventTypeChildChanged withBlock:^(FIRDataSnapshot *snapshot)
         {
             NSLog(@"AAA");
         }];
    }

    
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)applicationWillEnterForeground:(UIApplication *)application
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)applicationDidBecomeActive:(UIApplication *)application
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[FBSDKAppEvents activateApp];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	/*[OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken)
	{
		if (pushToken != nil)
			[UserDefaults setObject:userId forKey:@"OneSignalId"];
		else [UserDefaults removeObjectForKey:@"OneSignalId"];
		UpdateOneSignalId();
	}];*/
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[CacheManager cleanupExpired];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[NotificationCenter post:NOTIFICATION_APP_STARTED];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[self locationManagerStart];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)applicationWillTerminate:(UIApplication *)application
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [application cancelAllLocalNotifications];
    self.window.rootViewController = nil;

}

#pragma mark - Facebook responses

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url
												sourceApplication:sourceApplication annotation:annotation];
}


#pragma mark - Push notification methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSLog(@"AAA");
    [FIRMessaging messaging].APNSToken = deviceToken;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSLog(@"AAA");
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print full message.
    NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}



#pragma mark - Location manager methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)locationManagerStart
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (self.locationManager == nil)
	{
		self.locationManager = [[CLLocationManager alloc] init];
		[self.locationManager setDelegate:self];
		[self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
		[self.locationManager requestWhenInUseAuthorization];
	}
	[self.locationManager startUpdatingLocation];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)locationManagerStop
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[self.locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self.coordinate = newLocation.coordinate;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	
}

#pragma mark - Helper methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)initUserStatuses
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[self initUserStatus:@"Available"];
	[self initUserStatus:@"Busy"];
	[self initUserStatus:@"At school"];
	[self initUserStatus:@"At the movies"];
	[self initUserStatus:@"At work"];
	[self initUserStatus:@"Battery about to die"];
	[self initUserStatus:@"Can't talk now"];
	[self initUserStatus:@"In a meeting"];
	[self initUserStatus:@"At the gym"];
	[self initUserStatus:@"Sleeping"];
	[self initUserStatus:@"Urgent calls only"];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)initUserStatus:(NSString *)name
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	FObject *object = [FObject objectWithPath:FUSERSTATUS_PATH];
	object[FUSERSTATUS_NAME] = name;
	[object saveInBackground:^(NSError *error)
	{
		if (error != nil) NSLog(@"initUserStatus error: %@", error);
	}];
}


- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    
    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:@"token_fcm"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}



@end

