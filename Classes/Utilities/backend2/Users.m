//
// Copyright (c) 2016 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "utilities.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface Users()
{
	NSTimer *timer;
	BOOL refreshUserInterface;
	FIRDatabaseReference *firebase;
}
@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation Users

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (Users *)shared
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	static dispatch_once_t once;
	static Users *users;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	dispatch_once(&once, ^{ users = [[Users alloc] init]; });
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return users;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)init
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTIFICATION_APP_STARTED];
	[NotificationCenter addObserver:self selector:@selector(initObservers) name:NOTIFICATION_USER_LOGGED_IN];
	[NotificationCenter addObserver:self selector:@selector(actionCleanup) name:NOTIFICATION_USER_LOGGED_OUT];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	timer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(refreshUserInterface) userInfo:nil repeats:YES];
	[[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return self;
}

#pragma mark - Backend methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)initObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([FUser currentId] != nil)
	{
		if (firebase == nil) [self createObservers];
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)createObservers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
   firebase = [[[FIRDatabase database] referenceWithPath:FUSER_PATH] child:[FUser currentId]];
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
    [firebase observeEventType:FIRDataEventTypeChildChanged withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value != [NSNull null]){
            FUser *user = [FUser currentUser];
            
            user[snapshot.key] = snapshot.value;
            [user saveLocalIfCurrent];
        }
    }];
    
    
    [[firebase child:FFR_FRIENDS]  observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *friends)
    {
        if(friends.value != [NSNull null]){
            for ( FIRDataSnapshot *child in friends.children) {
                NSDictionary *friendDict = child.value;
                
                if(![[friendDict objectForKey:@"isWaiting"] boolValue] && [[friendDict objectForKey:@"isFriend"] boolValue] && ![[friendDict objectForKey:@"isBlocked"] boolValue]){
                    //Get USER
                    [[[[FIRDatabase database] referenceWithPath:FUSER_PATH] child:[friendDict objectForKey:@"objectId"]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull user) {
                        if(user.value != [NSNull null]){
                            dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
                                [self updateRealm:user.value];
                                refreshUserInterface = YES;
                            });
                        }
                        
                    }];
                }
            }
        }
	}];
	//---------------------------------------------------------------------------------------------------------------------------------------------
    //When other user log in or change his profile
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)updateRealm:(NSDictionary *)user
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	RLMRealm *realm = [RLMRealm defaultRealm];
	[realm beginWriteTransaction];
	[DBUser createOrUpdateInRealm:realm withValue:user];
	[realm commitWriteTransaction];
}

#pragma mark - Cleanup methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCleanup
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[firebase removeAllObservers]; firebase = nil;
}

#pragma mark - Notification methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)refreshUserInterface
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (refreshUserInterface)
	{
		[NotificationCenter post:NOTIFICATION_REFRESH_USERS];
		refreshUserInterface = NO;
	}
}

-(void) refreshUser:(void (^)(BOOL success)) completion{

    [[firebase child:FFR_FRIENDS]  observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *friends)
     {
         if(friends.value != [NSNull null]){
             for ( FIRDataSnapshot *child in friends.children) {
                 NSDictionary *friendDict = child.value;
                 
                 if(![[friendDict objectForKey:@"isWaiting"] boolValue] && [[friendDict objectForKey:@"isFriend"] boolValue] && ![[friendDict objectForKey:@"isBlocked"] boolValue]){
                     //Get USER
                     [[[[FIRDatabase database] referenceWithPath:FUSER_PATH] child:[friendDict objectForKey:@"objectId"]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull user) {
                         if(user.value != [NSNull null]){
                             dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
                                 [self updateRealm:user.value];
                                 refreshUserInterface = YES;
                             });
                         }
                         
                     }];
                 }else{
                     RLMRealm *realm = [RLMRealm defaultRealm];

                     [realm transactionWithBlock:^{
                         if([DBUser objectForPrimaryKey:friendDict[@"objectId"]]){
                             [realm deleteObject:[DBUser objectForPrimaryKey:friendDict[@"objectId"]]];
                         }
                         
                     }];
                 }
             }
             
             completion(YES);

         }else{
             completion(NO);
         }
     }];
}

@end

