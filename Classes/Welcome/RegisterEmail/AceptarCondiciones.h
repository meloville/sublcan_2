//
//  AceptarCondiciones.h
//  Subclan
//
//  Created by Carmelo Villegas cruz on 16/5/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "utilities.h"
#import "RegisterEmailView.h"



@interface AceptarCondiciones : UIViewController
@property (nonatomic, assign) IBOutlet id<RegisterEmailDelegate>delegate;
//@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet WKWebView *webview;

@property (strong, nonatomic) UITextField *fieldEmail;
@property (strong, nonatomic) UITextField *fieldPassword;

@end
