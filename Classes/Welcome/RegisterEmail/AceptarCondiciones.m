//
//  AceptarCondiciones.m
//  Subclan
//
//  Created by Carmelo Villegas cruz on 16/5/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "AceptarCondiciones.h"
#import "RegisterEmailView.h"

@interface AceptarCondiciones ()

@end

@implementation AceptarCondiciones

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *path = [Dir application:@"privacy.html"];
    [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ibaAceptar:(id)sender {
    /*NSString *email = [_fieldEmail.text lowercaseString];
    NSString *password = _fieldPassword.text;
    
    [Account add:email password:password];
    [self dismissViewControllerAnimated:YES completion:^{
        if (_delegate != nil) [delegate didRegisterUser];
    }];*/
    
    RegisterEmailView *parent = (RegisterEmailView*)self.presentingViewController;
       
    [self dismissViewControllerAnimated:YES completion:^{
        [parent ibaRegistrar];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
