//
//  FriendRequests.h
//  app
//
//  Created by Carmelo Villegas cruz on 3/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendRequests : UIViewController<UITextFieldDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *inputCode;
@property (weak, nonatomic) IBOutlet UITextView *inputDetails;

- (IBAction)ibaSendInvitation:(id)sender;
@end
