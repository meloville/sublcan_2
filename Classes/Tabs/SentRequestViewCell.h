//
//  SentRequestViewCell.h
//  Subclan
//
//  Created by Carmelo Villegas cruz on 04/02/2019.
//  Copyright © 2019 KZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"

NS_ASSUME_NONNULL_BEGIN

@interface SentRequestViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UITextView *requestDetails;
@property (weak, nonatomic) IBOutlet UIButton *iboRemoveRequest;
@property (weak, nonatomic) IBOutlet UIImageView *imageUser;


- (void)loadImage:(NSString*)urlImage;
@end

NS_ASSUME_NONNULL_END
