//
//  InformacionAyudaViewController.h
//  Subclan
//
//  Created by Carmelo Villegas cruz on 13/7/17.
//  Copyright © 2017 KZ. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface InformacionAyudaViewController : UIViewController<MFMailComposeViewControllerDelegate>

@end
