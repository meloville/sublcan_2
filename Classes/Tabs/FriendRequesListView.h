//
//  FriendRequesListView.h
//  app
//
//  Created by Carmelo Villegas cruz on 3/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendRequesListView : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end
