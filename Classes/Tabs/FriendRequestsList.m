//
//  FriendRequestsList.m
//  app
//
//  Created by Carmelo Villegas cruz on 3/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "FriendRequests.h"
#import "FriendRequest.h"
#import "RequestCell.h"
#import "NavigationController.h"
#import "FriendRequestsList.h"

@interface FriendRequestsList ()


    @property (nonatomic, strong) NSMutableArray *arrayRequest;
    @property (nonatomic, strong) FIRDatabaseReference *ref;
@end

@implementation FriendRequestsList

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    {
        [self.tabBarItem setImage:[UIImage imageNamed:@"tab_calls"]];
        self.tabBarItem.title = @"Invitar";
        
        
        //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"RequestCell"];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Invitar";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self
                             action:@selector(actionSendInvitation)];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    _ref = [[[_ref child:FUSER_PATH] child:[FUser currentId]] child:FFR_FRIENDS];
    
    
    _arrayRequest = [NSMutableArray array];
}

-(void) viewWillAppear:(BOOL)animated{
    [_ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if([snapshot hasChildren]){
            for(FIRDataSnapshot* request in snapshot.children){
                FriendRequest *fr = [[FriendRequest alloc] init];
                [fr setObjectId:[request.value objectForKey:@"objectId"]];
                [fr setIsBlocked:[[request.value objectForKey:@"isBlocked"] boolValue]];
                [fr setIsFriend:[[request.value objectForKey:@"isFriend"] boolValue]];
                [fr setIsWaiting:[[request.value objectForKey:@"isWaiting"] boolValue]];
                [fr setMessage:[request.value objectForKey:@"message"]];
                
                [_arrayRequest addObject:fr];
            }
        
        }
        [self.tableView reloadData];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrayRequest.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"RequestCell";
    RequestCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell)
    {
        cell = [[RequestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    FriendRequest *fr = [_arrayRequest objectAtIndex:indexPath.row];
        
    cell.userName.text = fr.objectId;
    cell.requestMessage.text = fr.message;
    //cell.backgroundColor = [UIColor redColor];
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - 

-(void) actionSendInvitation{
    FriendRequests *selectSingleView = [[FriendRequests alloc] init];
    
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:selectSingleView];
    [self presentViewController:navController animated:YES completion:nil];
}

@end
