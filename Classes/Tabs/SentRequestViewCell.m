//
//  SentRequestViewCell.m
//  Subclan
//
//  Created by Carmelo Villegas cruz on 04/02/2019.
//  Copyright © 2019 KZ. All rights reserved.
//

#import "SentRequestViewCell.h"

@implementation SentRequestViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_userName setText:@""];
    [_requestDetails setText:@""];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadImage:(NSString*)urlImage
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    _imageUser.layer.cornerRadius = _imageUser.frame.size.width/2;
    _imageUser.layer.masksToBounds = YES;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSString *path = [DownloadManager pathImage:urlImage];
    if (path == nil)
    {
        _imageUser.image = [UIImage imageNamed:@"people_blank"];
        [DownloadManager image:urlImage completion:^(NSString *path, NSError *error, BOOL network)
         {
             if (error == nil)
             {
                 _imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
             }
         }];
    }
    else
    {
        _imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
    }
}

@end
