//
//  RegisteredAccessViewController.m
//  app
//
//  Created by Carmelo Villegas cruz on 16/3/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "RegisteredAccessViewController.h"

@interface RegisteredAccessViewController ()
@property (weak, nonatomic) IBOutlet UITextField *iboFailedAttempts;
@property (weak, nonatomic) IBOutlet UITextField *iboLastFailedAttemptDate;

@end

@implementation RegisteredAccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view from its nib.
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger failedAttempts = [standardDefaults integerForKey:@"IntentosFallidos"];
    NSDate *date = [standardDefaults objectForKey:@"IntentosFallidosFecha"];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    [_iboFailedAttempts setText:[@(failedAttempts) stringValue]];
    if(date){ [_iboLastFailedAttemptDate setText:[formatter stringFromDate:date]]; }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
