//
//  SegurityViewController.m
//  app
//
//  Created by Carmelo Villegas cruz on 23/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "SecurityViewController.h"
#import "UserDefaults.h"
#import "AppConstant.h"
#import "SettingPasscodeViewController.h"
#import "RegisteredAccessViewController.h"
#import "CierreSeccionViewController.h"

//Passcode
#import <VENTouchLock/VENTouchLock.h>
#import "PasscodeViewController.h"

@interface SecurityViewController ()

@property (strong, nonatomic) IBOutletCollection(UISwitch) NSArray *iboSwitchCollection;
@property (weak, nonatomic) IBOutlet UISwitch *iboSwitchFive;
@property (weak, nonatomic) IBOutlet UISwitch *iboSwitchFifteen;
@property (weak, nonatomic) IBOutlet UISwitch *iboSwitchThirty;
@property (weak, nonatomic) IBOutlet UISwitch *iboSwitchFortyFive;
@property (weak, nonatomic) IBOutlet UISwitch *iboSwitchNever;
- (IBAction)ibaSwitch:(UISwitch*)sender;
- (IBAction)ibaPasscode :(UIButton*)sender;
@property (weak, nonatomic) IBOutlet UISwitch *iboSwitch;

@end

@implementation SecurityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Seguridad";
    self.navigationItem.title = @"Seguridad";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"chat_back"]
                                                                             style:UIBarButtonItemStylePlain target:self action:@selector(actionBack)];
    
    
    //Check if the time_session has been set.
    if([UserDefaults integerForKey:TIME_SESSION_KEY]){
        UISwitch *swt = [self switchFromValue:[UserDefaults integerForKey:TIME_SESSION_KEY]];
        [swt setOn:YES animated:YES];
        [self switchTo:NO allComponents:swt];
    }
    
    _iboSwitch.tintColor = [UIColor redColor];
    _iboSwitch.layer.cornerRadius = 16;
    _iboSwitch.backgroundColor =[UIColor redColor];
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"KEYCHAIN_NUMERO_DE_INTENTOS"]){
        NSNumber *intentos = [[NSUserDefaults standardUserDefaults] objectForKey:@"KEYCHAIN_NUMERO_DE_INTENTOS"];
        [_iboSwitch setOn:(intentos.integerValue==5)];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -- Actions

-(void) actionBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ibaSwitch:(UISwitch*)sender {
    NSInteger intentos = 10000;
    
    if([sender isOn]){
        intentos = 5;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:@(intentos) forKey:@"KEYCHAIN_NUMERO_DE_INTENTOS"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [VENTouchLock sharedInstance].passcodeAttemptLimit = intentos;
    
    /*if([sender isOn]){
        //Quiere decir que el Switch estaba apagado y tendemos a encenderlo
        [self switchTo:![sender isOn] allComponents:sender];
        [UserDefaults setObject:[self valueInSeconds:sender] forKey:TIME_SESSION_KEY];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TIME_SESSION
                                                            object:[self valueInSeconds:sender]];

    }else{
        BOOL someEnable = NO;
        for (UISwitch *swt in _iboSwitchCollection) {
            someEnable = someEnable || swt.isOn;
        }
        
        if(!someEnable){
            //Default value
            [_iboSwitchFive setOn:YES animated:YES];
            [UserDefaults setObject:[self valueInSeconds:_iboSwitchFive] forKey:TIME_SESSION_KEY];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TIME_SESSION
                                                                object:[self valueInSeconds:_iboSwitchFive]];
        }
    }*/
}

/*
 * Switch on/off the switches diffetents that sender
 */
-(void) switchTo:(BOOL)value allComponents:(UISwitch*)sender{
    for (UISwitch *swt in _iboSwitchCollection) {
        if(swt != sender){
            [swt setOn:value animated:YES];
        }
    }
}

/*
 * Get the value in seconds depends on the input sender.
 */
-(NSNumber*) valueInSeconds:(UISwitch*)sender{
    NSNumber *valueSeconds = @(-1);
    
    if(sender == _iboSwitchFive){
        valueSeconds = @(300);
    }else if(sender == _iboSwitchFifteen){
        valueSeconds = @(900);
    }else if(sender == _iboSwitchThirty){
        valueSeconds = @(1800);
    }else if(sender == _iboSwitchFortyFive){
        valueSeconds = @(2700);
    }

    return valueSeconds;
}

/*
 * Return the switch depends on the input value.
 */
-(UISwitch*) switchFromValue:(NSInteger)value{
    UISwitch *result = _iboSwitchFive;
    switch (value) {
        case 300:
            result = _iboSwitchFive;
            break;
        case 900:
            result= _iboSwitchFifteen;
            break;
        case 1800:
            result = _iboSwitchThirty;
            break;
        case 2700:
            result = _iboSwitchFortyFive;
            break;
        case -1:
            result = _iboSwitchNever;
            break;
        default:
            break;
    }
    return result;
}

#pragma mark - Cierre de seccion

- (IBAction)ibaCierreSeccion:(id)sender {
    CierreSeccionViewController* cierreView = [[CierreSeccionViewController alloc] init];
    [self.navigationController pushViewController:cierreView animated:YES];
}


#pragma mark - Passcode

- (IBAction)ibaPasscode :(UIButton*)sender{
    SettingPasscodeViewController *settingPasscode = [[SettingPasscodeViewController alloc] init];
    [self.navigationController pushViewController:settingPasscode animated:YES];
}

- (IBAction)ibaResgiteredAccess:(id)sender {
    RegisteredAccessViewController *registerAccess = [[RegisteredAccessViewController alloc] init];
    [self.navigationController pushViewController:registerAccess animated:YES];
}


@end
