//
//  DeleteAccountView.m
//  app
//
//  Created by Carmelo Villegas cruz on 8/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "SettingsView.h"
#import "DeleteAccountView.h"
#import <VENTouchLock/VENTouchLock.h>

@interface DeleteAccountView ()

@end

@implementation DeleteAccountView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Eliminar Cuenta";
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Volver" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)deleteAccount:(id)sender {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Eliminar cuenta"
                                 message:@"¿Estas seguro que deseas eliminar su cuenta?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Sí, por favor"
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self actionDeleteAccount];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No, gracias"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) actionDeleteAccount{    
    //1. Recuperamos sus chats.
    FIRDatabaseReference *reference = [[FIRDatabase database] referenceWithPath:FRECENT_PATH];
    FIRDatabaseQuery *query = [[reference queryOrderedByChild:FRECENT_USERID] queryEqualToValue:[FUser currentId]];
    
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
     {
         if(snapshot.value != [NSNull null]){
             
             NSMutableArray *arrayRecent = [NSMutableArray array];
             NSMutableArray *arrayChats = [NSMutableArray array];
             for (FIRDataSnapshot *recent in snapshot.children) {
                 [arrayRecent addObject:[recent.value objectForKey:@"objectId"]];
                 [arrayChats addObject:[recent.value objectForKey:@"groupId"]];
                 
                 NSString *groupID = [recent.value objectForKey:@"groupId"];
                 //Remove Recent
                 FIRDatabaseQuery *queryRecent = [[reference queryOrderedByChild:FRECENT_GROUPID] queryEqualToValue:[recent.value objectForKey:@"groupId"]];
                 [queryRecent observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshotRecent) {
                     if(snapshotRecent.value != [NSNull null]){
                         for (FIRDataSnapshot *obj in snapshotRecent.children){
                             [[[[FIRDatabase database] referenceWithPath:FRECENT_PATH] child:[obj.value objectForKey:@"objectId"]] removeValue];
                         }
                     }
                 }];
                 
                 //Remove Message
                 FIRDatabaseReference *messageRef = [[FIRDatabase database] referenceWithPath:FMESSAGE_PATH];
                 [[messageRef child:groupID] removeValue];
                 //TODO Recorrer todos los mensajes y eliminar las imagenes
                 
                 //Remove Typing
                 FIRDatabaseReference *typingRef = [[FIRDatabase database] referenceWithPath:FTYPING_PATH];
                 [[typingRef child:groupID] removeValue];
             }
         }
     }];
    
    //Borrando passcode
    [[VENTouchLock sharedInstance] deletePasscode];
    
    
    //Remove user
    NSString *userID = [FUser currentId];
    FIRDatabaseReference *userRef = [[[FIRDatabase database] referenceWithPath:FUSER_PATH] child:userID];
    
    [[userRef child:FFR_FRIENDS] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value != [NSNull null]){
            for (NSString *friendID in [snapshot.value allKeys]) {
                [[[[[[FIRDatabase database] referenceWithPath:FUSER_PATH] child:friendID] child:FFR_FRIENDS] child:[FUser currentId]] removeValue];
            }
        }
        
        [[[[FIRDatabase database] referenceWithPath:FUSER_PATH] child:userID] removeValue];
    }];
    
    [[FIRAuth auth].currentUser deleteWithCompletion:^(NSError * _Nullable error) {
        //[(SettingsView*)[self presentingViewController] actionLogoutUser];
        //[self.navigationController popViewControllerAnimated:YES];
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self.tabBarController setSelectedIndex:TAB_SETTING];
        LogoutUser(DEL_ACCOUNT_ONE);
        [ProgressHUD showError:@"Tu cuenta ha sido eliminada"];
    }];
    
}


@end
