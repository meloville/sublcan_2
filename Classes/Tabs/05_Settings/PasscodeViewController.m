//
//  PasscodeViewController.m
//  app
//
//  Created by Carmelo Villegas cruz on 15/3/17.
//  Copyright © 2017 KZ. All rights reserved.
//


#import "PasscodeViewController.h"
#import "utilities.h"



@interface PasscodeViewController ()

    @property (weak, nonatomic) IBOutlet UIButton *touchIDButton;

@end

@implementation PasscodeViewController

- (instancetype)init
{
    self = [super initWithNibName:NSStringFromClass([self class]) bundle:nil];
    if (self) {
        self.didFinishWithSuccess = ^(BOOL success, VENTouchLockSplashViewControllerUnlockType unlockType) {
            if (success) {                            
                NSString *logString = @"Sample App Unlocked";
                switch (unlockType) {
                    case VENTouchLockSplashViewControllerUnlockTypeTouchID: {
                        logString = [logString stringByAppendingString:@" con Touch ID."];
                        break;
                    }
                    case VENTouchLockSplashViewControllerUnlockTypePasscode: {
                        logString = [logString stringByAppendingString:@" con Passcode."];
                        break;
                    }
                    default:
                        break;
                }
                NSLog(@"%@", logString);
            }
            else {
                
                LogoutUser(DEL_ACCOUNT_ONE);
                
                UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
                
                
                
                
                while (topController.presentedViewController) {
                    topController = topController.presentedViewController;
                }
                
                LoginUser(topController);
            }
        };
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.touchIDButton.hidden = ![VENTouchLock shouldUseTouchID];
}

- (IBAction)userTappedShowTouchID:(id)sender
{
    [self showTouchID];
}

- (IBAction)userTappedEnterPasscode:(id)sender
{
    [self showPasscodeAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
