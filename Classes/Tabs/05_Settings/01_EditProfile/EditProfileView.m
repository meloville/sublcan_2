//
// Copyright (c) 2016 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "EditProfileView.h"
#import "CountriesView.h"
#import "NavigationController.h"
#import "StatusView.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface EditProfileView()
{
	BOOL isOnboard;
}

@property (strong, nonatomic) IBOutlet UIView *viewHeader;
@property (strong, nonatomic) IBOutlet UIImageView *imageUser;
@property (strong, nonatomic) IBOutlet UILabel *labelInitials;

@property (strong, nonatomic) IBOutlet UITableViewCell *cellFirstname;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellLastname;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellCountry;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellLocation;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellStatus;

@property (strong, nonatomic) IBOutlet UITextField *fieldFirstname;
@property (strong, nonatomic) IBOutlet UITextField *fieldLastname;

@property (strong, nonatomic) IBOutlet UILabel *labelPlaceholder;
@property (strong, nonatomic) IBOutlet UILabel *labelCountry;
@property (strong, nonatomic) IBOutlet UITextField *fieldLocation;
@property (strong, nonatomic) IBOutlet UIView *viewFooter;
@property (weak, nonatomic) IBOutlet UILabel *userID;

@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation EditProfileView

@synthesize viewHeader, imageUser, labelInitials, viewFooter;
@synthesize cellFirstname, cellLastname, cellCountry, cellLocation, cellStatus;
@synthesize fieldFirstname, fieldLastname;
@synthesize labelPlaceholder, labelCountry, fieldLocation, userID;

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWith:(BOOL)isOnboard_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	isOnboard = isOnboard_;
	return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewDidLoad];
	self.title = @"Editar Perfil    ";
	//---------------------------------------------------------------------------------------------------------------------------------------------
	
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Volver" style:UIBarButtonItemStylePlain target:self action:@selector(actionCancel)];
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Guardar" style:UIBarButtonItemStylePlain target:self action:@selector(actionDone)];
    
	//---------------------------------------------------------------------------------------------------------------------------------------------
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
	[self.tableView addGestureRecognizer:gestureRecognizer];
	gestureRecognizer.cancelsTouchesInView = NO;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	self.tableView.tableHeaderView = viewHeader;
    self.tableView.tableFooterView = viewFooter;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	//imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
    imageUser.layer.cornerRadius = 20;
	imageUser.layer.masksToBounds = YES;
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
    //userID.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //userID.layer.borderWidth = 1.0f;
    //userID.layer.cornerRadius = 5.0f;
    //userID.layer.masksToBounds = YES;
    
    /*[userID.layer setShadowColor:[UIColor blackColor].CGColor];
    [userID.layer setShadowOpacity:0.8];
    [userID.layer setShadowRadius:5.0];
    [userID.layer setShadowOffset:CGSizeMake(2.0, 2.0)];*/
    
    
	[self loadUser];
    
    // Register for the method to be called every time the time session expired
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(dismissCameraView:)
     name:NOTIFICATION_SESSION_TIMER
     object:nil
     ];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewWillDisappear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewWillDisappear:animated];
	[self dismissKeyboard];
}

- (void)viewDidAppear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [super viewDidAppear:animated];
    //[self loadUser];
    
    FUser *user = [FUser currentUser];
    if(cellStatus.textLabel.text != user[FUSER_STATUS]){
        cellStatus.textLabel.text = user[FUSER_STATUS];
    }
}


//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)dismissKeyboard
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[self.view endEditing:YES];
}

#pragma mark - Backend actions

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadUser
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	FUser *user = [FUser currentUser];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	labelInitials.text = [user initials];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[DownloadManager image:user[FUSER_PICTURE] completion:^(NSString *path, NSError *error, BOOL network)
	{
		if (error == nil)
		{
			imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
			labelInitials.text = nil;
		}
	}];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	fieldFirstname.text = user[FUSER_FIRSTNAME];
	fieldLastname.text = user[FUSER_LASTNAME];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	labelCountry.text = user[FUSER_COUNTRY];
	fieldLocation.text = user[FUSER_LOCATION];
    
    cellStatus.textLabel.text = user[FUSER_STATUS];

    
    userID.text = user[FUSER_OBJECTID];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[self updateViewDetails];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)saveUser
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	FUser *user = [FUser currentUser];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	NSString *fullname = [NSString stringWithFormat:@"%@ %@", fieldFirstname.text, fieldLastname.text];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	user[FUSER_FULLNAME] = fullname;
	user[FUSER_FIRSTNAME] = fieldFirstname.text;
	user[FUSER_LASTNAME] = fieldLastname.text;
	
    user[FUSER_COUNTRY] = @"";
    user[FUSER_LOCATION] = @"";
    //user[FUSER_COUNTRY] = labelCountry.text;
	//user[FUSER_LOCATION] = fieldLocation.text;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[user saveInBackground:^(NSError *error)
	{
		if (error != nil) [ProgressHUD showError:@"Network error."];
	}];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)saveUserPictures:(NSDictionary *)links
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	FUser *user = [FUser currentUser];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	user[FUSER_PICTURE] = links[@"linkPicture"];
	user[FUSER_THUMBNAIL] = links[@"linkThumbnail"];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[user saveInBackground:^(NSError *error)
	{
		if (error != nil) [ProgressHUD showError:@"Network error."];
	}];
}

#pragma mark - User actions

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCancel
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (isOnboard) LogoutUser(DEL_ACCOUNT_ALL);
    if(![self.navigationController popViewControllerAnimated:YES]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (void)actionStatus
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    StatusView *statusView = [[StatusView alloc] init];
    statusView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:statusView animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionDone
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([fieldFirstname.text length] == 0)	{	[ProgressHUD showError:@"Firstname must be set."];	return; }
	if ([fieldLastname.text length] == 0)	{	[ProgressHUD showError:@"Lastname must be set."];	return; }
	
    /*if ([labelCountry.text length] == 0)	{	[ProgressHUD showError:@"Country must be set."];	return; }
	if ([fieldLocation.text length] == 0)	{	[ProgressHUD showError:@"Location must be set."];	return; }*/
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[self saveUser];
    if(![self.navigationController popViewControllerAnimated:YES]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (IBAction)actionPhoto:(id)sender
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

	UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Abrir cámara" style:UIAlertActionStyleDefault
													handler:^(UIAlertAction *action) { PresentPhotoCamera(self, YES); }];
	UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Carrete" style:UIAlertActionStyleDefault
													handler:^(UIAlertAction *action) { PresentPhotoLibrary(self, YES); }];
    UIAlertAction *actionShowPhoto = [UIAlertAction actionWithTitle:@"Ver imagen actual" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action) { [self showPhoto]; }];
	UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];

	[alert addAction:action1];
    [alert addAction:action2];
    //[alert addAction:actionShowPhoto];
    [alert addAction:action3];
	[self presentViewController:alert animated:YES completion:nil];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCountries
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	CountriesView *countriesView = [[CountriesView alloc] init];
	countriesView.delegate = self;
	NavigationController *navController = [[NavigationController alloc] initWithRootViewController:countriesView];
	[self presentViewController:navController animated:YES completion:nil];
}


#pragma mark Show photos

-(void) showPhoto{
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:1];
    
    [self.navigationController pushViewController:browser animated:YES];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    NSInteger numberPhotos = 0;
    if(imageUser){
        numberPhotos = 1;
    }
    return numberPhotos;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    
    return [MWPhoto photoWithImage:imageUser.image];
}

#pragma mark - UIImagePickerControllerDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	UIImage *image = info[UIImagePickerControllerEditedImage];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	//UIImage *imagePicture = [Image square:image size:140];
    UIImage *imagePicture = [Image square:image size:1000];
	UIImage *imageThumbnail = [Image square:image size:60];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	//NSData *dataPicture = UIImageJPEGRepresentation(imagePicture, 0.6);
    NSData *dataPicture = UIImageJPEGRepresentation(imagePicture, 0.6);
	NSData *dataThumbnail = UIImageJPEGRepresentation(imageThumbnail, 0.6);
	//---------------------------------------------------------------------------------------------------------------------------------------------
	FIRStorage *storage = [FIRStorage storage];
	FIRStorageReference *reference1 = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(@"profile_picture", @"jpg")];
	FIRStorageReference *reference2 = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(@"profile_thumbnail", @"jpg")];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[reference1 putData:dataPicture metadata:nil completion:^(FIRStorageMetadata *metadata1, NSError *error)
	{
		if (error == nil)
		{
			[reference2 putData:dataThumbnail metadata:nil completion:^(FIRStorageMetadata *metadata2, NSError *error)
			{
				if (error == nil)
				{
					labelInitials.text = nil;
					imageUser.image = imagePicture;
					NSString *linkPicture = metadata1.downloadURL.absoluteString;
					NSString *linkThumbnail = metadata2.downloadURL.absoluteString;
					[self saveUserPictures:@{@"linkPicture":linkPicture, @"linkThumbnail":linkThumbnail}];
				}
				else [ProgressHUD showError:@"Network error."];
			}];
		}
		else [ProgressHUD showError:@"Network error."];
	}];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CountriesDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didSelectCountry:(NSString *)country CountryCode:(NSString *)countryCode
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	labelCountry.text = country;
	[fieldLocation becomeFirstResponder];
	[self updateViewDetails];
}

#pragma mark - Table view data source

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return 2;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if(section == 0) return 2;
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (section == 1) return @"Estado";
    return nil;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ((indexPath.section == 0) && (indexPath.row == 0)) return cellFirstname;
	if ((indexPath.section == 0) && (indexPath.row == 1)) return cellLastname;
    if ((indexPath.section == 1) && (indexPath.row == 0)) return cellStatus;
	if ((indexPath.section == 0) && (indexPath.row == 3)) return cellCountry;
	if ((indexPath.section == 0) && (indexPath.row == 4)) return cellLocation;
	return nil;
}

#pragma mark - Table view delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	if ((indexPath.section == 0) && (indexPath.row == 2)) [self actionCountries];
    if ((indexPath.section == 1) && (indexPath.row == 0)) [self actionStatus];

}

#pragma mark - UITextField delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)textFieldShouldReturn:(UITextField *)textField
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (textField == fieldFirstname)	[fieldLastname becomeFirstResponder];
	if (textField == fieldLastname)		[self actionCountries];
	if (textField == fieldLocation)		[self actionDone];
	return YES;
}


#pragma mark -- Copy method

- (IBAction)ibaCopyID:(id)sender {
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    
    NSString *textToCopy = [NSString stringWithFormat:@"¡Hola! \nPara descargarte Subclan, tu aplicación de chat oculto con álbum privado, sigue por favor el siguiente enlace (solo disponible para Iphone):\nUna vez descargada en tu teléfono móvil, introduce mi código de usuario en la pestaña \"Invitar\" y espera a que acepte tu solicitud de contacto.\nEste es mi código de usuario: %@\n\n¡Nos vemos en Subclan!", [userID text]];
    
    [pb setString:textToCopy];
    [ProgressHUD showSuccess:@"Se ha copiado el texto"];
    
}
#pragma mark - Helper methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)updateViewDetails
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	labelPlaceholder.hidden = (labelCountry.text != nil);
}


/* 
 * This method is thrown when the time session has expired. 
 * We create it to ensure that UIAlertViewController is shown.
 */
-(void) dismissCameraView:(NSNotification*) notification{
    if([self.presentedViewController isKindOfClass:[UIImagePickerController class]]){
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }else if([self.presentedViewController isKindOfClass:[UIAlertController class]]){
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

@end

