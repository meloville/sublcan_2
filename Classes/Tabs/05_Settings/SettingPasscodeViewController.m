//
//  SettingPasscodeViewController.m
//  app
//
//  Created by Carmelo Villegas cruz on 15/3/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "SettingPasscodeViewController.h"

@interface SettingPasscodeViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *touchIDSwitch;

@end

@implementation SettingPasscodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _touchIDSwitch.tintColor = [UIColor redColor];
    _touchIDSwitch.layer.cornerRadius = 16;
    _touchIDSwitch.backgroundColor = [UIColor redColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureTouchIDToggle];
}

- (void)configureTouchIDToggle
{
    self.touchIDSwitch.enabled = [[VENTouchLock sharedInstance] isPasscodeSet] && [VENTouchLock canUseTouchID];
    self.touchIDSwitch.on = [VENTouchLock shouldUseTouchID];
}

- (IBAction)userTappedSetPasscode:(id)sender
{
    if ([[VENTouchLock sharedInstance] isPasscodeSet]) {
        [[[UIAlertView alloc] initWithTitle:@"El passcode fue establecido" message:@"Si quieres añadir uno nuevo debes desactivar el actual" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else {
        VENTouchLockCreatePasscodeViewController *createPasscodeVC = [[VENTouchLockCreatePasscodeViewController alloc] init];
        [self presentViewController:[createPasscodeVC embeddedInNavigationController] animated:YES completion:nil];
    }
}

- (IBAction)userTappedShowPasscode:(id)sender
{
    if ([[VENTouchLock sharedInstance] isPasscodeSet]) {
        VENTouchLockEnterPasscodeViewController *showPasscodeVC = [[VENTouchLockEnterPasscodeViewController alloc] init];
        [self presentViewController:[showPasscodeVC embeddedInNavigationController] animated:YES completion:nil];
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Passcode" message:@"Por favor, añade un passcode" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (IBAction)userTappedDeletePasscode:(id)sender
{
    if ([[VENTouchLock sharedInstance] isPasscodeSet]) {
        [[VENTouchLock sharedInstance] deletePasscode];
        [self configureTouchIDToggle];
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Passcode" message:@"Desactivado el passcode" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (IBAction)userTappedSwitch:(UISwitch *)toggle
{
    [VENTouchLock setShouldUseTouchID:toggle.on];
}

@end
