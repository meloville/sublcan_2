//
//  MiCarreteViewController.h
//  Subclan
//
//  Created by Carmelo Villegas cruz on 23/6/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"


@interface MiCarreteViewController : UIViewController<MWPhotoBrowserDelegate>

@end
