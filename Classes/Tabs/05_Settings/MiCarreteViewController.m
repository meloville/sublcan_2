//
//  MiCarreteViewController.m
//  Subclan
//
//  Created by Carmelo Villegas cruz on 23/6/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "utilities.h"
#import "Cryptor.h"
#import "MiCarreteViewController.h"

@interface MiCarreteViewController ()
    @property (nonatomic, strong) NSMutableArray *images;
    @property (nonatomic, strong) MWPhotoBrowser *browser;
@end

@implementation MiCarreteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _images = [NSMutableArray array];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDeleted == NO AND type= %@", MESSAGE_PICTURE];
    RLMResults * dbmessages = [[DBMessage objectsWithPredicate:predicate] sortedResultsUsingKeyPath:FMESSAGE_CREATEDAT ascending:YES];
    
    for (DBMessage *message in dbmessages) {
        
        NSNumber *createAt = message[FMESSAGE_CREATEDAT];
        NSDate *dateCreate = [NSDate dateWithTimeIntervalSince1970:createAt.doubleValue];
        NSInteger days =[self daysBetweenDate:dateCreate andDate:[NSDate date]];
        
        if(days > 1){
            [Message deleteItem:message];
            FIRStorage *storage = [FIRStorage storage];
            FIRStorageReference *referenceStr = [storage referenceForURL:message[FMESSAGE_PICTURE]];
            [referenceStr deleteWithCompletion:^(NSError * _Nullable error) {}];
        }else{
            NSURL *url = [NSURL URLWithString:message[FMESSAGE_PICTURE]];
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            NSData *cryptedPicture = [Cryptor decryptData:data groupId:message[FMESSAGE_GROUPID]];
            UIImage *img = [[UIImage alloc] initWithData:cryptedPicture];
            
            
            [_images addObject:[MWPhoto photoWithImage:img]];
        }
        

    }

    
    
    [self foo];

}


-(void) foo{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    _browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    _browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    _browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    _browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    _browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    _browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    _browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    _browser.startOnGrid = YES; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    _browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //[self.navigationController pushViewController:_browser animated:YES];
    
    // Manipulate
    [_browser showNextPhotoAnimated:YES];
    [_browser showPreviousPhotoAnimated:YES];
    [_browser setCurrentPhotoIndex:10];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser{

    return [_images count];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index{
    if (index < _images.count) {
        return [_images objectAtIndex:index];
    }
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index{
    if (index < _images.count) {
        return [_images objectAtIndex:index];
    }
    return nil;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

@end
