//
//  FriendRequesListView.m
//  app
//
//  Created by Carmelo Villegas cruz on 3/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "FriendRequests.h"
#import "FriendRequest.h"
#import "RequestViewCell.h"
#import "NavigationController.h"
#import "FriendRequesListView.h"
#import "FriendRequestSentListViewViewController.h"


@interface FriendRequesListView ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

    @property (nonatomic, strong) NSMutableArray *arrayRequest;
    @property (nonatomic, strong) FIRDatabaseReference *ref;
@end

@implementation FriendRequesListView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    {
        [self.tabBarItem setImage:[UIImage imageNamed:@"tab_groups"]];
        self.tabBarItem.title = @"Solicitudes";
        
        
        [NotificationCenter addObserver:self selector:@selector(refreshBadget) name:NOTIFICATION_USER_LOGGED_IN];
        [NotificationCenter addObserver:self selector:@selector(actionCleanup) name:NOTIFICATION_USER_LOGGED_OUT];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self
                      action:@selector(actionSendInvitation)];
    
    
    /*self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks
          target:self
          action:@selector(actionSentRequest)];
     */
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    _ref = [[FIRDatabase database] reference];
    
    _arrayRequest = [NSMutableArray array];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"RequestViewCell" bundle:nil] forCellReuseIdentifier:@"RequestViewCell"];
    
    [self.tableView setAllowsSelection:NO];
    
    [self refreshBadget];
}


- (void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"Volver";
}


- (void) refreshBadget{
    if([FUser currentId] != nil){
        FIRDatabaseReference *reference = [[[_ref child:FUSER_PATH] child:[FUser currentId]] child:FFR_FRIENDS];
        [reference observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            self.tabBarItem.badgeValue = nil;
            if([snapshot hasChildren]){
                for(FIRDataSnapshot* request in snapshot.children){
                    FriendRequest *fr = [[FriendRequest alloc] init];
                    [fr setObjectId:[request.value objectForKey:@"objectId"]];
                    [fr setIsBlocked:[[request.value objectForKey:@"isBlocked"] boolValue]];
                    [fr setIsFriend:[[request.value objectForKey:@"isFriend"] boolValue]];
                    [fr setIsWaiting:[[request.value objectForKey:@"isWaiting"] boolValue]];
                    [fr setMessage:[request.value objectForKey:@"message"]];
                    
                    if([fr isWaiting]){
                        
                        NSNumber *badgetValue = [NSNumber numberWithInteger:([self.tabBarItem.badgeValue integerValue] + 1)];
                        if(badgetValue > 0){
                            self.tabBarItem.badgeValue = [badgetValue stringValue];
                        }else{
                            self.tabBarItem.badgeValue = nil;
                        }                        
                    }
                }
                
            }
            [self reloadFriendRequest];
        }];
    }

}

-(void) viewWillAppear:(BOOL)animated{    
    //self.navigationItem.title = @"Solicitudes recibidas";
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Recibidas",@"Enviadas"]];
    [segmentedControl setSelectedSegmentIndex:0];
    
    [segmentedControl addTarget:self
                         action:@selector(actionSegmentButton:)
               forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = segmentedControl;
    
    [self reloadFriendRequest];
}

-(void) actionSegmentButton:(UISegmentedControl*)element{
    if(element.selectedSegmentIndex == 1){
        [self actionSentRequest];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrayRequest.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __block RequestViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RequestViewCell" forIndexPath:indexPath];
    
    [cell.iboAcceptRequest addTarget:self action:@selector(acceptRequest:) forControlEvents:UIControlEventTouchUpInside];
    [cell.iboRejectRequest addTarget:self action:@selector(rejectRequest:) forControlEvents:UIControlEventTouchUpInside];
    
    FriendRequest *fr = [_arrayRequest objectAtIndex:indexPath.row];
    
    FIRDatabaseReference *refUser = [[[[FIRDatabase database] reference] child:FUSER_PATH] child:fr.objectId];
    
    //cell.userName.text = @"aa";
    [refUser observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if(snapshot.value != [NSNull null]){
            cell.userName.text = [snapshot.value objectForKey:@"fullname"];
            cell.userStatus.text = [snapshot.value objectForKey:@"status"];
            cell.requestDetails.text = fr.message;
            
            [cell loadImage:[snapshot.value objectForKey:@"picture"]];            
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
    
    }];
    
    
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}


 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
     // Return NO if you do not want the specified item to be editable.
     return NO;
 }

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendRequest *fr = [_arrayRequest objectAtIndex:indexPath.row];
    
    UITableViewRowAction *delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Rechazar" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        // Delete something here
                                        [self rejectFriendRequest:fr indexPath:indexPath];
                                    }];
    delete.backgroundColor = [UIColor redColor];
    
    UITableViewRowAction *more = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Aceptar " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                  {
                                      [self acceptFriendRequest:fr indexPath:indexPath];
                                      
                                  }];
    more.backgroundColor = [UIColor colorWithRed:0.188 green:0.514 blue:0.984 alpha:1];
    
    return @[delete, more]; //array with all the buttons you want. 1,2,3, etc...
}

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Table view delegate
 
 // In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 // Navigation logic may go here, for example:
 // Create the next view controller.
 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
 
 // Pass the selected object to the new view controller.
 
 // Push the view controller.
 [self.navigationController pushViewController:detailViewController animated:YES];
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark -

-(void) actionSendInvitation{
    FriendRequests *selectSingleView = [[FriendRequests alloc] init];    
    [self.navigationController pushViewController:selectSingleView animated:YES];
}

-(void) actionSentRequest{
    FriendRequestSentListViewViewController *viewController = [[FriendRequestSentListViewViewController alloc] init];
    
    [self.navigationController pushViewController:viewController animated:YES];
}



/*
 * Este método es llamado cuando pulsamos rechazar la petición.
 */
-(void)rejectRequest:(UIButton*)button{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Sublcan"
                                                                   message:@"¿Estas seguro de que quieres eliminar la petición de amistad seleccionada?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) {
                                                              NSIndexPath *indexPath = [self.tableView indexPathForCell:(RequestViewCell*)[button superview].superview];
                                                              FriendRequest *fr = [_arrayRequest objectAtIndex:indexPath.row];
                                                              [self rejectFriendRequest:fr indexPath:indexPath];
                                                          }];
    
    UIAlertAction* cancelACtion = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:defaultAction];
    [alert addAction:cancelACtion];
    [self presentViewController:alert animated:YES completion:nil];    
}


/*
 * Este método es llamado cuando pulsamos ACEPTAR la petición.
 */
-(void)acceptRequest:(UIButton*)button{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:(RequestViewCell*)[button superview].superview];
    @try {
        FriendRequest *fr = [_arrayRequest objectAtIndex:indexPath.row];
        [self acceptFriendRequest:fr indexPath:indexPath];
    } @catch (NSException *exception) {
        
        
        
        NSString *stringError = @"El Array es null ....";
        if (!_arrayRequest){
            stringError = [NSString stringWithFormat:@"%s \n IndexPath: %ld \nArray %lu",
            "Se ha producido un error, envie este mensaje al desarrollador: ", (long)indexPath.row, (unsigned long)[_arrayRequest count]];
        }        
        
        //TODO Deberiamos ver que error está dando
        //[ProgressHUD showError:exception.description];
        //[ProgressHUD showError:stringError];
    } @finally {
        
    }

}





-(void) rejectFriendRequest:(FriendRequest*)fr indexPath:(NSIndexPath*)indexPath{
    FIRDatabaseReference *reference = [[[_ref child:FUSER_PATH] child:[FUser currentId]] child:FFR_FRIENDS];
    [reference child:fr.objectId];
    @try {
        [reference removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            [_arrayRequest removeObject:fr];
            //[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
        }];
    } @catch (NSException *exception) {
        NSLog(@"");
    } @finally {        
    }

}

-(void) acceptFriendRequest:(FriendRequest*)fr  indexPath:(NSIndexPath*)indexPath{
    FIRDatabaseReference *reference = [[[_ref child:FUSER_PATH] child:[FUser currentId]] child:FFR_FRIENDS];
    [[reference child:fr.objectId] updateChildValues:@{@"isFriend" : @(YES),
                                   @"isWaiting" : @(NO)}
             withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                 //We have to include the current user as friend to the request user.
                 
                 FIRDatabaseReference *acceptRef = [[[[_ref child:FUSER_PATH] child:fr.objectId] child:FFR_FRIENDS] child:[FUser currentId]];
                 [acceptRef updateChildValues:@{@"isFriend"     : @(YES),
                                                  @"isWaiting"  : @(NO),
                                                  @"isBlocked"  : @(NO),
                                                  @"message"    : @"",
                                                  @"objectId"   : [FUser currentId]}];
                 
                 
                 [_arrayRequest removeObject:fr];
                 //[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
             }];
    
    
}

#pragma mark - 

- (void)actionCleanup
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [NotificationCenter removeObserver:self];
    [_ref removeAllObservers]; _ref = nil;
}

-(void) reloadFriendRequest{
    if(_ref == nil){
        _ref = [[FIRDatabase database] reference];
    }
    
    if([FUser currentId] != nil){
        FIRDatabaseReference *reference = [[[_ref child:FUSER_PATH] child:[FUser currentId]] child:FFR_FRIENDS];
        [reference observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            [_arrayRequest removeAllObjects];
            if([snapshot hasChildren]){
                for(FIRDataSnapshot* request in snapshot.children){
                    FriendRequest *fr = [[FriendRequest alloc] init];
                    [fr setObjectId:[request.value objectForKey:@"objectId"]];
                    [fr setIsBlocked:[[request.value objectForKey:@"isBlocked"] boolValue]];
                    [fr setIsFriend:[[request.value objectForKey:@"isFriend"] boolValue]];
                    [fr setIsWaiting:[[request.value objectForKey:@"isWaiting"] boolValue]];
                    [fr setMessage:[request.value objectForKey:@"message"]];
                    
                    if([fr isWaiting]){
                        [_arrayRequest addObject:fr];
                    }
                }
                
            }
            if(_arrayRequest.count > 0){
                self.tabBarItem.badgeValue = [@(_arrayRequest.count) stringValue];
            }else{
                self.tabBarItem.badgeValue = nil;
            }
            
            [self.tableView reloadData];
        }];
    }
}


@end
