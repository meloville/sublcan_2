//
//  SettingsViewController.m
//  Subclan
//
//  Created by Carmelo Villegas cruz on 12/7/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "SettingsViewController.h"
#import "TermsView.h"
#import "MiCuentaViewController.h"
#import "MiCarreteViewController.h"
#import "EditProfileView.h"

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageUser;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;

@property (strong, nonatomic) NSMutableArray* photos;
@end

@implementation SettingsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self.tabBarItem setImage:[UIImage imageNamed:@"tab_settings"]];
        self.tabBarItem.title = @"Configuración";
        //-----------------------------------------------------------------------------------------------------------------------------------------
        [NotificationCenter addObserver:self selector:@selector(loadUser) name:NOTIFICATION_USER_LOGGED_IN];
        [NotificationCenter addObserver:self selector:@selector(actionCleanup) name:NOTIFICATION_USER_LOGGED_OUT];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Configuración";

    
    _imageUser.layer.cornerRadius = 10;
    _imageUser.layer.masksToBounds = YES;
    [_imageUser.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [_imageUser.layer setBorderWidth: 1.0];
    
    
    [_iboBtnID setTitle:@"TEST" forState:UIControlStateNormal];
    [_iboBtnID setTitle:[FUser currentUser].objectId forState:UIControlStateNormal];
    [_iboBtnID.titleLabel setTextColor:[UIColor blackColor]];
    _iboBtnID.layer.borderWidth = 2.0f;
    _iboBtnID.layer.cornerRadius = 10; 
    _iboBtnID.layer.borderColor = [UIColor colorWithRed:177/255.0f green:149/255.0f blue:58/255.0f alpha:1].CGColor;
    _iboBtnID.clipsToBounds = YES;

    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    if (![FUser isOnboardOk]){
        OnboardUser(self);
    }
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_imageUser setImage:nil];
    [_labelName setText:@""];
    [_labelName setText:@""];
    
    //TODO BORRRAR IMAGEN Y TEXTOS
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [_iboBtnID setTitle:[FUser currentUser].objectId forState:UIControlStateNormal];
    if ([FUser currentId] != nil)
    {
        [self loadUser];
    }
    else LoginUser(self);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)loadUser{
    FUser *user = [FUser currentUser];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [DownloadManager image:user[FUSER_PICTURE] completion:^(NSString *path, NSError *error, BOOL network)
     {
         if (error == nil)
         {
             _imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
         }
     }];
    
    _labelName.text = user[FUSER_FULLNAME];
    _labelStatus.text = user[FUSER_STATUS];
    
    
}

- (void)actionCleanup
{
    
    
}


#pragma mark - Lee aqui

- (IBAction)ibaLeeAqui:(id)sender {
    TermsView *termsView = [[TermsView alloc] init];
    [termsView setContentHTML:@"leeAqui.html"];
    termsView.hidesBottomBarWhenPushed = YES;
    [termsView setTitle:@"Lee Aquí"];
    [self.navigationController pushViewController:termsView animated:YES];
}


#pragma mark - Mi Cuenta

- (IBAction)ibaMiCuenta:(id)sender {
    MiCuentaViewController *miCuentaView = [[MiCuentaViewController alloc] init];
    [miCuentaView setTitle:@"Mi Cuenta"];
    [self.navigationController pushViewController:miCuentaView animated:YES];
    
}


- (IBAction)ibaMiAlbum:(id)sender {
    /*MiCarreteViewController *statusView = [[MiCarreteViewController alloc] init];
    statusView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:statusView animated:YES];*/
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDeleted == NO AND type= %@", MESSAGE_PICTURE];
    RLMResults * dbmessages = [[DBMessage objectsWithPredicate:predicate] sortedResultsUsingKeyPath:FMESSAGE_CREATEDAT ascending:YES];
    
    _photos = [NSMutableArray array];
    for (DBMessage *message in dbmessages) {
        
        NSNumber *createAt = message[FMESSAGE_CREATEDAT];
        NSDate *dateCreate = [NSDate dateWithTimeIntervalSince1970:createAt.doubleValue];
        NSInteger days =[self daysBetweenDate:dateCreate andDate:[NSDate date]];
        
        if(days > 1){
            [Message deleteItem:message];
            FIRStorage *storage = [FIRStorage storage];
            FIRStorageReference *referenceStr = [storage referenceForURL:message[FMESSAGE_PICTURE]];
            [referenceStr deleteWithCompletion:^(NSError * _Nullable error) {}];
        }else{
            NSURL *url = [NSURL URLWithString:message[FMESSAGE_PICTURE]];
            NSData *data = [NSData dataWithContentsOfURL:url];
            
            NSData *cryptedPicture = [Cryptor decryptData:data groupId:message[FMESSAGE_GROUPID]];
            UIImage *img = [[UIImage alloc] initWithData:cryptedPicture];
            
            
            [_photos addObject:[MWPhoto photoWithImage:img]];
        }
    }
    
    MWPhotoBrowser * browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = YES; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    [self.navigationController pushViewController:browser animated:YES];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:10];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser{
    
    return [_photos count];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index{
    if (index < _photos.count) {
        return [_photos objectAtIndex:index];
    }
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index{
    if (index < _photos.count) {
        return [_photos objectAtIndex:index];
    }
    return nil;
}

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

#pragma mark - Editar perfil

- (IBAction)ibaEditarPerfil:(id)sender {
    EditProfileView *editProfileView = [[EditProfileView alloc] initWith:NO];
    
    [self.navigationController pushViewController:editProfileView animated:YES];
}

#pragma mark -

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ibaCopyID:(id)sender {
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    
    NSString *textToCopy = [NSString stringWithFormat:@"¡Hola! \nPara descargarte Subclan, tu aplicación de chat oculto con álbum privado, sigue por favor el siguiente enlace (solo disponible para Iphone):\nUna vez descargada en tu teléfono móvil, introduce mi código de usuario en la pestaña \"Invitar\" y espera a que acepte tu solicitud de contacto.\nEste es mi código de usuario: %@\n\n¡Nos vemos en Subclan!", [FUser currentUser].objectId];
    
    [pb setString:textToCopy];
    [ProgressHUD showSuccess:@"Se ha copiado el texto"];
}


@end
