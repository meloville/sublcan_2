//
//  SettingsViewController.h
//  Subclan
//
//  Created by Carmelo Villegas cruz on 12/7/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"

@interface SettingsViewController : UIViewController<MWPhotoBrowserDelegate>
@property (weak, nonatomic) IBOutlet UIButton *iboBtnID;
- (IBAction)ibaCopyID:(id)sender;

@end
