//
// Copyright (c) 2016 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "ChatsCell.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface ChatsCell()


@property (strong, nonatomic) IBOutlet UILabel *labelInitials;

@property (strong, nonatomic) IBOutlet UILabel *labelDescription;
@property (strong, nonatomic) IBOutlet UILabel *labelLastMessage;

@property (strong, nonatomic) IBOutlet UILabel *labelElapsed;
@property (strong, nonatomic) IBOutlet UILabel *labelCounter;

@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation ChatsCell

@synthesize imageUser, labelInitials;
@synthesize labelDescription, labelLastMessage;
@synthesize labelElapsed, labelCounter;


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
    imageUser.layer.masksToBounds = YES;
}

/*
 @property NSString *objectId;
 
 @property NSString *userId;
 @property NSString *name;
 @property NSString *picture;
 @property NSString *members;
 
 @property BOOL isDeleted;
 
 @property NSTimeInterval createdAt;
 @property NSTimeInterval updatedAt;
 */
- (void)bindDataGroup:(DBGroup *)dbgroup lastMessage:(NSString*)lastMessage lastMessageDate:(NSTimeInterval)lastMessageDate groupId:(NSString*)groupId counter:(NSUInteger)counter{
    labelDescription.text = dbgroup.name;
    
    labelCounter.text = @"";
    labelElapsed.text = @"";
    labelLastMessage.text = @"";
    /* Image */
    NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: dbgroup.picture]];
    imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
    imageUser.layer.masksToBounds = YES;
    if(data){
        imageUser.image = [UIImage imageWithData: data];;
        labelInitials.text = nil;
        [imageUser setNeedsDisplay];
    }
    
    if(lastMessage != nil){
        [self addLastMessagelastMessage:lastMessage lastMessageDate:lastMessageDate groupId:groupId counter:counter];
    }
    
}

-(void) addLastMessagelastMessage:(NSString*)lastMessage lastMessageDate:(NSTimeInterval)lastMessageDate groupId:(NSString*)groupId counter:(NSUInteger)counter{
    NSString *cryptedMessage = lastMessage;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
        NSString *lastMessage = [Cryptor decryptText:cryptedMessage groupId:groupId];
        dispatch_async(dispatch_get_main_queue(), ^{
            labelLastMessage.text = lastMessage;
        });
    });
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:lastMessageDate];
    NSTimeInterval seconds = [[NSDate date] timeIntervalSinceDate:date];
    labelElapsed.text = TimeElapsed(seconds);
    //---------------------------------------------------------------------------------------------------------------------------------------------
    labelCounter.text = (counter != 0) ? [NSString stringWithFormat:@"%ld new", (long) counter] : nil;
    
}


- (void)bindData:(DBRecent *)dbrecent TableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath{
	labelDescription.text = dbrecent.description;
    if([dbrecent.type isEqualToString:@"private"]){
        NSArray *members = [dbrecent.members componentsSeparatedByString:@","];
        for (NSString *member in members) {
            if(![member isEqualToString:[FUser currentId]]){
                FIRDatabaseReference *ref = [[[FIRDatabase database] referenceWithPath:FUSER_PATH] child:member];
                [ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    if(snapshot.value != [NSNull null]){
                        RLMRealm *realm = [RLMRealm defaultRealm];
                        [realm beginWriteTransaction];
                        [DBRecent createOrUpdateInRealm:realm withValue:@{FRECENT_OBJECTID: dbrecent.objectId,
                                                                          FRECENT_DESCRIPTION: snapshot.value[FUSER_FULLNAME],
                                                                          FRECENT_PICTURE : snapshot.value[FUSER_PICTURE]?snapshot.value[FUSER_PICTURE]:@""
                                                                          }];
                        [realm commitWriteTransaction];
                        
                        if(![dbrecent.description isEqualToString:snapshot.value[FUSER_FULLNAME]]){
                            labelDescription.text = snapshot.value[FUSER_FULLNAME];
                            [labelDescription setNeedsDisplay];
                        }else if(![dbrecent.picture isEqualToString:snapshot.value[FUSER_PICTURE]]){
                            NSString *path = [DownloadManager pathImage:snapshot.value[FUSER_PICTURE]];
                            if(path != nil){
                                imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
                                labelInitials.text = nil;
                                [imageUser setNeedsDisplay];
                            }else{
                                imageUser.image = [UIImage imageNamed:@"chats_blank"];
                                labelInitials.text = dbrecent.initials;
                            }
                        }
                        
                        [self loadImage:dbrecent TableView:tableView IndexPath:indexPath];
                    }else{
                        //El usuario con el que se compartiar el chat ya no existe.
                        [self userDontExistUpdateRealmFirebase:dbrecent key:snapshot.key];
                        
                        //Repasamos el resto de elementos.
                        for (DBRecent* recent in [DBRecent allObjects]) {
                            NSMutableArray *arrayMembers = [NSMutableArray arrayWithArray:[recent.members componentsSeparatedByString:@","]];
                            
                            if([arrayMembers containsObject:snapshot.key]){
                                [self userDontExistUpdateRealmFirebase:recent key:snapshot.key];
                            }
                        }
                        
                    }
                    
                    

                }];
            }
        }
    }else if([dbrecent.type isEqualToString:@"multiple"]){
        imageUser.image = [UIImage imageNamed:@"group_chat"];
        
        imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
        imageUser.layer.masksToBounds = YES;
        //labelInitials.text = @"GR";
    }if([dbrecent.type isEqualToString:@"group"]){
        /*NSLog(@"");
        NSString *path = [DownloadManager pathImage:dbrecent.picture];
        if(path != nil){
            imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
            labelInitials.text = nil;
            [imageUser setNeedsDisplay];
        }*/
    }
    
    
	//---------------------------------------------------------------------------------------------------------------------------------------------
	NSString *groupId = dbrecent.groupId;
	NSString *cryptedMessage = dbrecent.lastMessage;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
		NSString *lastMessage = [Cryptor decryptText:cryptedMessage groupId:groupId];
		dispatch_async(dispatch_get_main_queue(), ^{
			labelLastMessage.text = lastMessage;
		});
	});
	//---------------------------------------------------------------------------------------------------------------------------------------------
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:dbrecent.lastMessageDate];
	NSTimeInterval seconds = [[NSDate date] timeIntervalSinceDate:date];
	labelElapsed.text = TimeElapsed(seconds);
	//---------------------------------------------------------------------------------------------------------------------------------------------
	labelCounter.text = (dbrecent.counter != 0) ? [NSString stringWithFormat:@"%ld new", (long) dbrecent.counter] : nil;
}

- (void)loadImageGroup:(DBGroup *)dbgroup TableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath{
    imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
    imageUser.layer.masksToBounds = YES;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSString *path = [DownloadManager pathImage:dbgroup.picture];
    if (path == nil)
    {
        //imageUser.image = [UIImage imageNamed:@"chats_blank"];
        //labelInitials.text = dbrecent.initials;
        //[self downloadImage:dbrecent TableView:tableView IndexPath:indexPath];
    }
    else
    {
        imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
        labelInitials.text = nil;
    }
}


/*
 * Actualiza tanto Firebase como Realm cuando nos encontramos con un chat con un miembro que ya no existe
 */
- (void) userDontExistUpdateRealmFirebase:(DBRecent *)dbrecent key:(NSString*)userID {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    if([dbrecent.type isEqual:CHAT_PRIVATE]){
        //1. Eliminamos de firebase el Recent
        [[[[FIRDatabase database] referenceWithPath:FRECENT_PATH] child:dbrecent.objectId] removeValue];
        
        //1. También de Realm
        [realm transactionWithBlock:^{
            [realm deleteObject:[DBRecent objectForPrimaryKey:dbrecent.objectId]];
        }];
    }
    //Si es un grupo, tendríamos que borrar bien él como miembro o si es administrador el grupo entero
    else if([dbrecent.type isEqual:CHAT_GROUP]){
        if([dbrecent.userId isEqualToString:userID]){
            //El creador del grupo ya no existe, borramos todo.
            [realm transactionWithBlock:^{
                [realm deleteObject:[DBGroup objectForPrimaryKey:dbrecent.groupId]];
                
            }];
            [[[[FIRDatabase database] referenceWithPath:FGROUP_PATH] child:dbrecent.groupId] removeValue];
        }else{
            //Actualizamos sus miembros.
            NSMutableArray *arrayMembers = [NSMutableArray arrayWithArray:[dbrecent.members componentsSeparatedByString:@","]];
            [arrayMembers removeObject:userID];
            [realm transactionWithBlock:^{
                [DBGroup createOrUpdateInRealm:realm withValue:@{FGROUP_OBJECTID: dbrecent.groupId,
                                                                 FGROUP_OBJECTID: [arrayMembers componentsJoinedByString:@","]
                                                                 }];
            }];
            
            [[[[FIRDatabase database] referenceWithPath:FGROUP_PATH] child:dbrecent.groupId] setValue:arrayMembers];
        }
        
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadImage:(DBRecent *)dbrecent TableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
	imageUser.layer.masksToBounds = YES;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	NSString *path = [DownloadManager pathImage:dbrecent.picture];
	if (path == nil)
	{
        if([dbrecent.type isEqualToString:CHAT_PRIVATE]){
            imageUser.image = [UIImage imageNamed:@"chats_blank"];
            labelInitials.text = dbrecent.initials;
            [self downloadImage:dbrecent TableView:tableView IndexPath:indexPath];
        }else if([dbrecent.type isEqualToString:CHAT_GROUP]){            
            NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: dbrecent.picture]];
            if(data){
                imageUser.image = [UIImage imageWithData: data];;
                labelInitials.text = nil;
            }
        }
	}
	else
	{
		imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
		labelInitials.text = nil;
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)downloadImage:(DBRecent *)dbrecent TableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[DownloadManager image:dbrecent.picture completion:^(NSString *path, NSError *error, BOOL network)
	{
		if ((error == nil) && ([tableView.indexPathsForVisibleRows containsObject:indexPath]))
		{
			ChatsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
			cell.imageUser.image = [[UIImage alloc] initWithContentsOfFile:path];
			cell.labelInitials.text = nil;
		}
	}];
}

@end

