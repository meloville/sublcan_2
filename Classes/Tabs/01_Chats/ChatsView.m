//
// Copyright (c) 2016 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "ChatsView.h"
#import "ChatsCell.h"
#import "ChatView.h"
#import "SelectSingleView.h"
#import "NavigationController.h"
#import "UserDefaults.h"

#import "CreateGroupView.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface ChatsView()
{
    RLMResults *dbrecents;
    SWTableViewCell *lastCell;
}

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSTimer *closeSessionTimer;
@property (strong, nonatomic) NSTimer *countDownTimer;

@property (nonatomic, assign) int seconds;

@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation ChatsView

@synthesize searchBar;

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    {
        [self.tabBarItem setImage:[UIImage imageNamed:@"tab_chats"]];
        self.tabBarItem.title = @"Chats";
        //-----------------------------------------------------------------------------------------------------------------------------------------
        [NotificationCenter addObserver:self selector:@selector(actionCleanup) name:NOTIFICATION_USER_LOGGED_OUT];
        [NotificationCenter addObserver:self selector:@selector(refreshTableView) name:NOTIFICATION_REFRESH_RECENTS];
        [NotificationCenter addObserver:self selector:@selector(updateTimerNotification:) name:NOTIFICATION_REFRESH_TIME_SESSION];
        [NotificationCenter addObserver:self selector:@selector(loadProfile) name:NOTIFICATION_USER_LOGGED_REGISTER];

        
        
        _countDownTimer = [[NSTimer alloc] init];
        _seconds = 30;
        
    }
    return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [super viewDidLoad];
    self.title = @"Chats";
    //---------------------------------------------------------------------------------------------------------------------------------------------
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self
                                                                                           action:@selector(actionCompose)];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self.tableView registerNib:[UINib nibWithNibName:@"ChatsCell" bundle:nil] forCellReuseIdentifier:@"ChatsCell"];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    self.tableView.tableFooterView = [[UIView alloc] init];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self loadRecents];
    
    [self.closeSessionTimer invalidate];
    [self.countDownTimer invalidate];
    if([FUser currentId] != nil){
        //Usuario logado.
        self.closeSessionTimer = [self createSessionTimer];
    }
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];    
}


- (void)viewDidAppear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [super viewDidAppear:animated];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([FUser currentId] != nil)
    {
        if ([FUser isOnboardOk]){
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                OnboardUser(self);
             });
            
        }
        
        if(![_closeSessionTimer isValid]){
            [_countDownTimer invalidate];
            self.closeSessionTimer = [self createSessionTimer];
        }
        [self loadRecents];
        
    }
    else{
        //Vamos a la pantalla de login
        [self.navigationController popToRootViewControllerAnimated:YES];
        LoginUser(self);
    }
}

#pragma mark - Realm methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadRecents
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSString *text = searchBar.text;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isArchived == NO AND isDeleted == NO"];
    if(![text isEqualToString:@""]){
        predicate = [NSPredicate predicateWithFormat:@"isArchived == NO AND isDeleted == NO AND description CONTAINS[c] %@", text];
    }
    dbrecents = [[DBRecent objectsWithPredicate:predicate] sortedResultsUsingKeyPath:FRECENT_LASTMESSAGEDATE ascending:NO];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self refreshTableView];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)archiveRecent:(DBRecent *)dbrecent
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    dbrecent.isArchived = YES;
    [realm commitWriteTransaction];
}


-(void) abandonGroup:(NSString*)groupId recent:(NSString*)recentID{
    FObject *object = [FObject objectWithPath:FGROUP_PATH];
    object[FGROUP_OBJECTID] = groupId;
    
    
    
    DBGroup *dbgroup = [DBGroup objectForPrimaryKey:groupId];
    
    
    
    if(dbgroup != nil){

        //Check if the user is the owner
        if([dbgroup.userId isEqualToString:[FUser currentId]]){
            //If the user is the member, we have to delete the group so we have to find
            //all the recents
            
            [Group deleteItem:groupId];
            [Recent fetchRecents:groupId completion:^(NSMutableArray *recents) {
                for (FObject* recentGroup in recents) {
                    [Recent deleteItem:recentGroup.objectId];
                }
            }];
            
        }else{
            //The user is a member, so we remove their id from member in group and
            //"delete" the recent messeage.
            NSArray* arrayMembers = [dbgroup.members componentsSeparatedByString:@","];
            NSMutableArray *arraymutableMembers = [NSMutableArray arrayWithArray:arrayMembers];
            [arraymutableMembers removeObject:[FUser currentId]];
            
            object[FGROUP_MEMBERS] = arraymutableMembers;
            [object updateInBackground:^(NSError * _Nullable error) {
                [Recent updateMembers:object];
                
                [Recent fetchRecents:groupId completion:^(NSMutableArray *recents) {
                    for (FObject* recentGroup in recents) {
                        if([recentGroup.dictionary[FRECENT_USERID] isEqualToString:[FUser currentId]]){
                            [Recent deleteItem:recentGroup.objectId];
                        }
                    }
                }];
            }];


            
            MessageSend1 *message = [[MessageSend1 alloc] initWith:groupId View:nil];
            [message send:[NSString stringWithFormat:@"%@ ha abandonado el grupo", [FUser currentUser][FUSER_FULLNAME]] Video:nil Picture:nil Audio:nil];
            
            //[Recent deleteItem:recentID];
            /*FObject *recent = [FObject objectWithPath:FRECENT_PATH];
            recent[FRECENT_OBJECTID] = recentID;
            recent[FRECENT_ISDELETED] = @YES;
            [recent updateInBackground];*/
        }

        
        NSLog(@"PARA AQUi");
    }
    
    //[realm commitWriteTransaction];
}

-(void) deleteGroup:(NSString*)objectId{
    FObject *object = [FObject objectWithPath:FGROUP_PATH];
    object[FGROUP_OBJECTID] = objectId;
    [object deleteInBackground:nil];
    
    FObject *messages = [FObject objectWithPath:FMESSAGE_PATH];
    messages[FMESSAGE_OBJECTID] = objectId;
    [messages deleteInBackground:nil];
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    DBGroup *dbgroup = [DBGroup objectForPrimaryKey:objectId];
    if(dbgroup != nil){
        [realm deleteObject:dbgroup];
    }
    
    [realm commitWriteTransaction];
}


- (void)deleteRecent:(DBRecent *)dbrecent
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    dbrecent.isDeleted = YES;
    [realm commitWriteTransaction];
}

#pragma mark - Refresh methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)refreshTableView
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.tableView reloadData];
    [self refreshTabCounter];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)refreshTabCounter
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSInteger total = 0;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    for (DBRecent *dbrecent in dbrecents)
        total += dbrecent.counter;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    UITabBarItem *item = self.tabBarController.tabBar.items[3];
    item.badgeValue = (total != 0) ? [NSString stringWithFormat:@"%ld", (long) total] : nil;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    UIUserNotificationSettings *currentUserNotificationSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    if (currentUserNotificationSettings.types & UIUserNotificationTypeBadge)
        [UIApplication sharedApplication].applicationIconBadgeNumber = total>0?1:0;
        //[UIApplication sharedApplication].applicationIconBadgeNumber = 1;
}

#pragma mark - User actions

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionChat:(NSDictionary *)dictionary
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    ChatView *chatView = [[ChatView alloc] initWith:dictionary];
    chatView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatView animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCompose
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Chat" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { [self actionSelectSingle]; }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Grupo" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { [self actionSelectMultiple]; }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:action1]; [alert addAction:action2]; [alert addAction:action3];
    [self presentViewController:alert animated:YES completion:nil];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionSelectSingle
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    SelectSingleView *selectSingleView = [[SelectSingleView alloc] init];
    selectSingleView.delegate = self;
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:selectSingleView];
    [self presentViewController:navController animated:YES completion:nil];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionSelectMultiple
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    CreateGroupView *createGroupView = [[CreateGroupView alloc] init];
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:createGroupView];
    [self presentViewController:navController animated:YES completion:nil];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionArchive:(NSInteger)index
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    DBRecent *dbrecent = dbrecents[index];
    NSString *recentId = dbrecent.objectId;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self archiveRecent:dbrecent];
    [self refreshTabCounter];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self performSelector:@selector(delayedArchive:) withObject:recentId afterDelay:0.25];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)delayedArchive:(NSString *)recentId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.tableView reloadData];
    [Recent archiveItem:recentId];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionDelete:(NSInteger)index
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    DBRecent *dbrecent = dbrecents[index];
    NSString *recentId = dbrecent.objectId;
    
    if([dbrecent.type isEqualToString:CHAT_GROUP]){
        
        /*MessageSend1 *messageSend1 = [[MessageSend1 alloc] initWith:dbrecent.groupId View:self.navigationController.view];
        
        [messageSend1 send:[NSString stringWithFormat:@"%@ ha abandonado el chat", [FUser fullname]]
                     Video:nil
                   Picture:nil
                     Audio:nil];
        
        [messageSend1 send_leave_group:[NSString stringWithFormat:@"%@ ha abandonado el chat", [FUser fullname]]];*/

       // [self deleteGroup:dbrecent.groupId];
        
        [self abandonGroup:dbrecent.groupId recent:dbrecent.objectId];
    }
    
    [self deleteRecent:dbrecent];
    [self refreshTabCounter];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [self performSelector:@selector(delayedDelete:) withObject:recentId afterDelay:0.25];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)delayedDelete:(NSString *)recentId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.tableView reloadData];
    [Recent deleteItem:recentId];
}

#pragma mark - SelectSingleDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didSelectSingleUser:(DBUser *)dbuser2
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDictionary *dictionary = StartPrivateChat(dbuser2);
    [self actionChat:dictionary];
}

#pragma mark - Cleanup methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCleanup
{
    [self refreshTableView];
    
    [_closeSessionTimer invalidate];
    [_countDownTimer invalidate];
}

-(void) loadProfile{
    if ([FUser currentId] != nil)
    {
        if (![FUser isOnboardOk]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                OnboardUser(self);
             });
            
        }
    }
}

#pragma mark - UIScrollViewDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.view endEditing:YES];
}

#pragma mark - Table view data source

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return 1;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return [dbrecents count];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    ChatsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatsCell" forIndexPath:indexPath];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    cell.leftUtilityButtons = nil;
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
    cell.tag = indexPath.row;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //[cell bindData:dbrecents[indexPath.row]];
    [cell bindData:dbrecents[indexPath.row] TableView:tableView IndexPath:indexPath];
    [cell loadImage:dbrecents[indexPath.row] TableView:tableView IndexPath:indexPath];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSArray *)rightButtons
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    //[rightUtilityButtons sw_addUtilityButtonWithColor:HEXCOLOR(0x3E70A7FF) title:@"Archive"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor redColor] title:@"Eliminar"];
    return rightUtilityButtons;
}

#pragma mark - SWTableViewCellDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [cell hideUtilityButtonsAnimated:YES];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if (index == 0) [self actionDelete:cell.tag];
    if (index == 1) [self actionDelete:cell.tag];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (state == kCellStateRight) lastCell = cell;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return YES;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return YES;
}

#pragma mark - Table view delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if ((lastCell == nil) || [lastCell isUtilityButtonsHidden])
    {
        DBRecent *dbrecent = dbrecents[indexPath.row];
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithDictionary:RestartRecentChat(dbrecent)];
        
        NSString *path = [DownloadManager pathImage:dbrecent.picture];
        if (path != nil)
        {
            [dictionary setObject:[[UIImage alloc] initWithContentsOfFile:path] forKey:@"image"];
        }
        
        [ProgressHUD show];
        
        if([dbrecent.type isEqualToString:CHAT_GROUP]){
            ChatsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [dictionary setObject:cell.imageUser.image forKey:@"image"];
            [self actionChat:dictionary];
            [ProgressHUD dismiss];
        }else{
            //Check if the user is my friend
            [[[FIRDatabase database] referenceWithPath:[NSString stringWithFormat:FFR_PATH, [FUser currentId]]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if(snapshot.value != [NSNull null]){
                    for (NSString *member in [dictionary objectForKey:@"members"]) {
                        if(![member isEqualToString:[FUser currentId]]){
                            
                            //Check if the user is friend
                            if([snapshot.value objectForKey:member] == nil){
                                [self actionDelete:indexPath.row];
                                [ProgressHUD showError:@"Usuario no es tu amigo por lo que el chat se borrará" Interaction:YES];
                                break;
                            }
                            
                            FIRDatabaseReference *reference = [[FIRDatabase database] referenceWithPath:[NSString stringWithFormat:FFR_PATH, member]];
                            
                            [reference observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                                if(snapshot.value != [NSNull null]){
                                    if([[[snapshot.value objectForKey:[FUser currentId]] objectForKey:@"isBlocked"] boolValue]){
                                        NSLog(@"User is blocked");
                                        if([[dictionary objectForKey:@"type"] isEqualToString:@"group"]){
                                            [ProgressHUD showError:@"Algún miembro del grupo te ha bloqueado por lo que no puedes ver acceder a él" Interaction:YES];
                                        }else{
                                            [ProgressHUD showError:@"El usuario te ha bloqueado" Interaction:YES];
                                        }
                                        
                                    }else{
                                        [ProgressHUD dismiss];
                                        NSLog(@"User is a really good friend");
                                        [self actionChat:dictionary];
                                    }
                                }else{
                                    [self actionDelete:indexPath.row];
                                    [ProgressHUD showError:@"No eres amigo del usuario seleccionado, por favor enviale de nuevo una solicitud de amistad" Interaction:YES];
                                }
                            }];       
                        }
                    }
                }else{
                    [self actionDelete:indexPath.row];
                    [ProgressHUD showError:@"No eres amigo del usuario seleccionado, por favor enviale de nuevo una solicitud de amistad" Interaction:YES];
                }
            }];
            
            
            
            
        }
    }
    else [lastCell hideUtilityButtonsAnimated:YES];
}

#pragma mark - UISearchBarDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self loadRecents];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [searchBar setShowsCancelButton:YES animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [searchBar setShowsCancelButton:NO animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [self loadRecents];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [searchBar resignFirstResponder];
}


#pragma mark Close Timer

-(NSTimer*) createSessionTimer{
    NSTimer *timer = nil;
    if([UserDefaults integerForKey:TIME_SESSION_KEY] != TIME_SESSION_NEVER){
        timer = [NSTimer scheduledTimerWithTimeInterval:[UserDefaults integerForKey:TIME_SESSION_KEY] target:self selector:@selector(closeTimer:) userInfo:nil repeats:YES];
    }
    
    return timer;
}


/*
 * Notification
 */
-(void) updateTimerNotification:(NSNotification*)notification{
    [_closeSessionTimer invalidate];
    [_countDownTimer invalidate];
    //TODO Dismiss Alert
    
    _closeSessionTimer = [self createSessionTimer];
    
}

- (void) closeTimer:(NSTimer *)sender{
    _seconds = 30;
    
    NSString *message = [NSString stringWithFormat:@"La App se cerrará en %i segundos", _seconds];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cerrar sessión" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelLogoutOut = [UIAlertAction actionWithTitle:@"Continuar usando" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [_countDownTimer invalidate];
    }];
    
    
    [alert addAction:cancelLogoutOut];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SESSION_TIMER object:nil];
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [self presentViewController:alert animated:YES completion:nil];
        _countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateCountDownTimer:) userInfo:alert repeats:YES];
    });
}

-(void) updateCountDownTimer:(NSTimer*) timer{
    UIAlertController *alert = timer.userInfo;
    
    NSLog(@"Seconds %i", --_seconds);
    alert.message = [NSString stringWithFormat:@"La App se cerrará en %i segundos", _seconds];
    
    if(_seconds <= 0){
        [_countDownTimer invalidate];
        [timer invalidate];
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        LogoutUser(DEL_ACCOUNT_ONE);
        if(self.tabBarController.selectedIndex == DEFAULT_TAB){
            LoginUser(self);
        }else{
            [self.tabBarController setSelectedIndex:DEFAULT_TAB];
        }
    }
    
}

@end

