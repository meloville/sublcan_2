//
//  RequestCell.m
//  app
//
//  Created by Carmelo Villegas cruz on 3/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "RequestCell.h"

@implementation RequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
