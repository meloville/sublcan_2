//
//  FriendRequestSentListViewViewController.m
//  Subclan
//
//  Created by Carmelo Villegas cruz on 21/3/18.
//  Copyright © 2018 KZ. All rights reserved.
//

#import "FriendRequestSentListViewViewController.h"
#import "RequestViewCell.h"
#import "SentRequestViewCell.h"
#import "utilities.h"

@interface FriendRequestSentListViewViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayRequest;
@property (nonatomic, strong) FIRDatabaseReference *ref;

@end

@implementation FriendRequestSentListViewViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(_ref == nil){
        _ref = [[FIRDatabase database] reference];
    }
    
    self.title = @"Solicitudes enviadas";
    //self.navigationItem.title = @"Solicitudes enviadas";
    
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Recibidas",@"Enviadas"]];
    [segmentedControl setSelectedSegmentIndex:1];
    
    [segmentedControl addTarget:self
                         action:@selector(actionSegmentButton:)
               forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = segmentedControl;
    
    [self.navigationItem setHidesBackButton:YES];
    
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SentRequestViewCell" bundle:nil] forCellReuseIdentifier:@"SentRequestViewCell"];
    
    [self.tableView setAllowsSelection:NO];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView setAllowsSelection:NO];
    

    
    _arrayRequest = [[NSMutableArray alloc] init];

    [ProgressHUD show];
    FIRDatabaseReference *reference = [_ref child:FUSER_PATH];    
    [reference observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot){
            for (NSString *userID in [snapshot.value allKeys]) {
                for (NSDictionary *friends in [[[snapshot.value objectForKey:userID] objectForKey:FFR_FRIENDS] allValues]) {
                    if([[friends objectForKey:FUSER_OBJECTID] isEqualToString:[FUser currentId]]){                                                
                        if([[friends objectForKey:@"isWaiting"] boolValue]){
                            [_arrayRequest addObject:@{FUSER_OBJECTID: userID, @"message": [friends objectForKey:@"message"]}];
                            [_tableView reloadData];
                        }
                    }
                }
            }
            
            [ProgressHUD dismiss];

        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrayRequest.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __block SentRequestViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SentRequestViewCell" forIndexPath:indexPath];
    
    [cell.iboRemoveRequest addTarget:self action:@selector(removeRequest:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *data = [_arrayRequest objectAtIndex:indexPath.row];
    FIRDatabaseReference *refUser = [[[[FIRDatabase database] reference] child:FUSER_PATH] child:[data objectForKey:@"objectId"]];
    
    //cell.userName.text = @"aa";
    [refUser observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        if(snapshot.value != [NSNull null]){
            cell.userName.text = [snapshot.value objectForKey:@"fullname"];
            cell.requestDetails.text = [data objectForKey:@"message"];
            
            [cell loadImage:[snapshot.value objectForKey:@"picture"]];
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
        
    }];
    
    
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
    
    
    /*static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell != nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *data = [_arrayRequest objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [data objectForKey:FUSER_OBJECTID];
    cell.detailTextLabel.text = [data objectForKey:@"message"];
    
    
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    cell.textLabel.textColor = [UIColor colorWithRed:101/255.0 green:82/255.0 blue:23/255.0 alpha:1];
    
    cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    cell.detailTextLabel.textColor = [UIColor colorWithRed:177.0/255.0 green:149.0/255.0 blue:58.0/255.0 alpha:1];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;*/
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}


#pragma mark -

/*
 * Este método es llamado cuando pulsamos rechazar la petición.
 */
-(void)removeRequest:(UIButton*)button{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Sublcan"
                                                                   message:@"¿Estas seguro de que quieres eliminar la petición de amistad enviada?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDestructive
                                                            handler:^(UIAlertAction * action) {
                                                              NSIndexPath *indexPath = [self.tableView indexPathForCell:(RequestViewCell*)[button superview].superview];
                                                              
                                                                NSDictionary *data = [_arrayRequest objectAtIndex:indexPath.row];
                                                                NSString* targetUID = [data objectForKey:@"objectId"];
                                                                NSString* sourceUID = [FUser currentUser].objectId;
                                                                
                                                                
                                                                FIRDatabaseReference *refUser = [[[[[[FIRDatabase database] reference] child:FUSER_PATH] child:targetUID] child:FFR_FRIENDS] child:sourceUID];
                                                                
                                                                [refUser removeValue];
                                                                [_arrayRequest removeObject:data];
                                                                [self.tableView reloadData];
                                                          }];
    
    UIAlertAction* cancelACtion = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:defaultAction];
    [alert addAction:cancelACtion];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) actionSegmentButton:(UISegmentedControl*)element{
    if(element.selectedSegmentIndex == 0){
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
