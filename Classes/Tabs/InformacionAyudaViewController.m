//
//  InformacionAyudaViewController.m
//  Subclan
//
//  Created by Carmelo Villegas cruz on 13/7/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "InformacionAyudaViewController.h"
#import "TermsView.h"

@interface InformacionAyudaViewController ()

@end

@implementation InformacionAyudaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- Aviso Legal

- (IBAction)ibaAvisoLegal:(id)sender {
    TermsView *termsView = [[TermsView alloc] init];
    [termsView setContentHTML:@"privacy.html"];
    [termsView setTitle:@"Aviso Legal"];
    termsView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:termsView animated:YES];
}


#pragma mark -- Información

- (IBAction)ibaInformacion:(id)sender {
    TermsView *termsView = [[TermsView alloc] init];
    [termsView setContentHTML:@"leeAqui.html"];
    [termsView setTitle:@"Información"];
    termsView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:termsView animated:YES];
}

#pragma mark - Contactanos

- (IBAction)ibaContactacnos:(id)sender {
    if ([MFMailComposeViewController canSendMail])
    {
        //NSString *emailTitle = @"Contáctanos";
        NSString *emailTitle = @"";
        NSString *messageBody = @"Por favor describe el motivo por el cual nos contactas";
        NSArray *toRecipents = [NSArray arrayWithObject:@"soporte@subclan.com"];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
