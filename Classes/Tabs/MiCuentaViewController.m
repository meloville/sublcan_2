//
//  MiCuentaViewController.m
//  Subclan
//
//  Created by Carmelo Villegas cruz on 12/7/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "MiCuentaViewController.h"
#import "SecurityViewController.h"
#import "DeleteAccountView.h"
#import "InformacionAyudaViewController.h"
#import "BlockedPeopleView.h"


@interface MiCuentaViewController ()

@end

@implementation MiCuentaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Privacidad
- (IBAction)ibaPrivacidad:(id)sender {
    BlockedPeopleView *blockPeopleView = [[BlockedPeopleView alloc] init];
    [self.navigationController pushViewController:blockPeopleView animated:YES];
}

#pragma mark - Informacion y ayuda

- (IBAction)ibaInformacionAyuda:(id)sender {
    InformacionAyudaViewController *infoAyudaView = [[InformacionAyudaViewController alloc] init];
    [infoAyudaView setTitle:@"Información y Ayuda"];
    [self.navigationController pushViewController:infoAyudaView animated:YES];
}


#pragma mark - Seguridad

- (IBAction)ibaSeguridad:(id)sender {
    SecurityViewController *securityView = [[SecurityViewController alloc] init];
    
    [securityView setTitle:@"Cuentas Bloqueadas"];
    [self.navigationController pushViewController:securityView animated:YES];
}

#pragma mark - Eliminar Cuenta

- (IBAction)ibaEliminarCuenta:(id)sender {
    DeleteAccountView *deleteView = [[DeleteAccountView alloc] init];    
    [self.navigationController pushViewController:deleteView animated:YES];
}

#pragma mark -
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
