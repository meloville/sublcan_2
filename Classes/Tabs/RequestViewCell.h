//
//  RequestViewCell.h
//  app
//
//  Created by Carmelo Villegas cruz on 3/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"


@interface RequestViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userStatus;
@property (weak, nonatomic) IBOutlet UITextView *requestDetails;
@property (weak, nonatomic) IBOutlet UIButton *iboRejectRequest;
@property (weak, nonatomic) IBOutlet UIButton *iboAcceptRequest;
@property (weak, nonatomic) IBOutlet UIImageView *imageUser;


- (void)loadImage:(NSString*)urlImage;
@end
