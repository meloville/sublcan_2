//
//  FriendRequests.m
//  app
//
//  Created by Carmelo Villegas cruz on 3/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import "utilities.h"
#import "FriendRequests.h"
#import "UITextView+Placeholder.h"

@interface FriendRequests ()

@end

@implementation FriendRequests

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    {
        [self.tabBarItem setImage:[UIImage imageNamed:@"tb_invitar"]];
        self.tabBarItem.title = @"Invitar";

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Invitar";
    
    self.navigationItem.title = @"Invitar";

    [_inputCode setDelegate:self];
    [_inputDetails setDelegate:self];
    
    //_inputDetails.placeholder = @"Texto";
    //_inputDetails.placeholderColor = [UIColor lightGrayColor]; // optional
    
    //[self addDoneToolBarToKeyboard:_inputDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Send Invitation

- (IBAction)ibaSendInvitation:(id)sender {
    
    //We have to check if the input user code exists.
    #pragma mark TODO
    

    [_inputCode resignFirstResponder];
    [_inputDetails resignFirstResponder];
    
    
    __block NSString *inputDetails = _inputDetails.text;
    
    if(_inputCode.text.length < 28){
        [ProgressHUD showError:@"Código de envió erróneo"];
    }else if([_inputCode.text isEqualToString:[FUser currentId]]){
        [ProgressHUD showError:@"No se puede enviar una petición a si mismo"];
    }else{
        
        FIRDatabaseReference *refUser = [[FIRDatabase database] reference];
        refUser = [[refUser child:FUSER_PATH] child:_inputCode.text];
        //Comprobamos si existe el usuario en la base de datos.
        [refUser observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if([snapshot.value isEqual:[NSNull null]]){
                [ProgressHUD showError:@"El código enviado no pertenece a ningún usuario."];
            }else{
                //Comprobar si ya está en su lista de amigos
                NSString *pathIsAlreadyFriend = [NSString stringWithFormat:FFR_PATH, [FUser currentId]];
                FIRDatabaseReference *refIsAlreadyFriend = [[FIRDatabase database] reference];
                refIsAlreadyFriend = [[refIsAlreadyFriend child:pathIsAlreadyFriend] child:_inputCode.text];
                
                [refIsAlreadyFriend observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    if([snapshot.value isEqual:[NSNull null]]){
                        //Si existe el usuario comprobamos si ya ha sido enviada o ha sido rechazada.
                        NSString *path = [NSString stringWithFormat:FFR_PATH, _inputCode.text];
                        FIRDatabaseReference *ref = [[FIRDatabase database] reference];
                        ref = [[ref child:path] child:[FUser currentId]];
                        
                        
                        [ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                            if([snapshot.value isEqual:[NSNull null]]){
                                //La petición de invitación no existe, por lo que creamos una nueva
                                NSDictionary *post = @{@"objectId": [FUser currentId],
                                                       @"message" : inputDetails,
                                                       @"isFriend": @NO,
                                                       @"isBlocked": @NO,
                                                       @"isWaiting": @YES
                                                       };
                                
                                //Creamos la petición.
                                [snapshot.ref updateChildValues:post withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                                    
                                    
                                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                    hud.mode = MBProgressHUDModeText;
                                    hud.labelText = @"Invitación enviada";
                                    
                                    [_inputDetails setText:@""];
                                    [_inputCode setText:@""];
                                    [hud show:YES];
                                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC);
                                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                        [hud hide:YES];
                                    });
                                }];
                            }else{
                                //Aquí quiere decir que la petición ya ha sido enviada anteriormente.
                                [ProgressHUD showError:@"La petición ya fue enviada anteriormente."];
                                
                            }
                        } withCancelBlock:^(NSError * _Nonnull error) {
                            NSLog(@"a");
                        }];
                    }else{
                        [ProgressHUD showError:@"El código pertenece a uno de tus contactos."];
                    }
                }];

            }
        } withCancelBlock:^(NSError * _Nonnull error) {
            [ProgressHUD showError:@"Se ha producido un error al enviar la petición."];
        }];

        
        
        /*[ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if([snapshot.value isEqual:[NSNull null]]){
         
            }
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"a");
        }];*/
        
        /*
        [ref updateChildValues:post withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = @"Invitación enviada";
            
            [_inputDetails setText:@""];
            [_inputCode setText:@""];
            [hud show:YES];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [hud hide:YES];
            });
        }];
         */
        
        
        _inputDetails.text = @"";
        [self actionBack];
    }
}

#pragma mark --

-(void) actionBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Keyboard

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField != _inputCode){
        [self ibaSendInvitation:nil];
    } else{
        [_inputDetails becomeFirstResponder];
    }
    return YES;
}

-(void)addDoneToolBarToKeyboard:(UITextView *)textView
{
    UIToolbar* doneToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    doneToolbar.barStyle = UIBarStyleBlackTranslucent;
    doneToolbar.items = [NSArray arrayWithObjects:
                         [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClickedDismissKeyboard)],
                         nil];
    [doneToolbar sizeToFit];
    textView.inputAccessoryView = doneToolbar;
}

//remember to set your text view delegate
//but if you only have 1 text view in your view controller
//you can simply change currentTextField to the name of your text view
//and ignore this textViewDidBeginEditing delegate method
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    _inputDetails = textView;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

-(void)doneButtonClickedDismissKeyboard
{
    [_inputDetails resignFirstResponder];
}




@end
