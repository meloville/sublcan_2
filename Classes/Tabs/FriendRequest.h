//
//  FriendRequest.h
//  app
//
//  Created by Carmelo Villegas cruz on 3/2/17.
//  Copyright © 2017 KZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendRequest : NSObject

    @property (nonatomic, assign) BOOL isBlocked;
    @property (nonatomic, assign) BOOL isFriend;
    @property (nonatomic, assign) BOOL isWaiting;
    @property (nonatomic, strong) NSString *message;
    @property (nonatomic, strong) NSString *objectId;

@end
